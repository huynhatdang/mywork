var user = {
    login: 'LOGIN_SUCESS',
    register: 'REGISTER_SUCCESS',
    signup: 'CREATE_SUCCESS',
    changePassword: 'CHANGE_SUCCESS',
    sendEmail: 'SEND_SUCCESS',
    getRank:'GET_SUCESS'
};

var team = {
    create: 'CREATE_TEAM_SUCCESS',
    addMember: 'ADD_MEMBER_SUCCESS',
    removeMember: 'REMOVE_MEMBER_SUCCESS',
    removeTeam: 'REMOVE_TEAM_SUCCESS',
    getSuccess: 'GET_TEAM_SUCCESS',
    update: 'UPDATE_TEAM_SUCCESS',
    joinTeam: 'REQUEST_JOINTEAM_SUCCESS'    
};

var pitch = {
    create: 'CREATE_PITCH_SUCCESS',
    get: 'GET_PITCH_SUCCESS',
    update: 'UPDATE_PITCH_SUCCESS',
    delete: 'DELETE_PITCH_SUCCESS',
    addPrice: 'ADD_PRICE_SUCCESS',
    updatePrice: 'UPDATE_PRICE_SUCESS',
    removePrice: 'REMOVE_PRICE_SUCCESS',
    addFeedback: 'ADD_FEEDBACK_SUCCESS',
    updateFeedback: 'UPDATE_FEEDBACK_SUCCESS',
    removeFeedback: 'REMOVE_FEEDBACK_SUCCESS'
};
var company = {
    create: 'CREATE_COMPANY_SUCCESS',
    get: 'GET_COMPANY_SUCCESS',
    update: 'UPDATE_COMPANY_SUCCESS',
    delete: 'DELETE_COMPANY_SUCCESS'
};
var cv = {
    create: 'CREATE_CV_SUCCESS',
    get: 'GET_CV_SUCCESS',
    update: 'UPDATE_CV_SUCCESS',
    delete: 'DELETE_CV_SUCCESS'
};
var work = {
    create: 'CREATE_WORK_SUCCESS',
    get: 'GET_WORK_SUCCESS',
    update: 'UPDATE_WORK_SUCCESS',
    delete: 'DELETE_WORK_SUCCESS'
};
var match = {
    create: 'CREATE_MATCH_SUCCESS',
    get: 'GET_MATCH_SUCCESS',
    update: 'UPDATE_MATCH_SUCCESS',
    remove: 'REMOVE_MATCH_SUCCESS',
    accept: 'ACCEPT_CHALLENGER_SUCCESS',
};
module.exports = {
    user: user,
    team: team,
    pitch: pitch,
    match: match,
    company: company,
    work: work,
    cv: cv
};