/*jshint esversion: 6 */
var router = require('express').Router();
var companyDao = require('./../dao/company.dao');
var middlewareJwt = require('../middlewares/jwt');
var multipart = require('connect-multiparty');
var fs = require('fs')
var multipartMiddleware = multipart();
var uploadSingle = require('../middlewares/uploadSingle');


module.exports = function () {
  router.post('/create', middlewareJwt.authentication(["company"]), createCompany);
  router.post('/upload', middlewareJwt.authentication(["company"]), uploadSingle.uploadSingle, uploadImage);
  router.get('/get/:id', getCompany);
  router.put('/update/:id', updateCompany);
  router.delete('/delete/:id', deleteCompany);


  function createCompany(req, res, next) {

    // console.log(req.files.file);

    // var file = req.files.file;
    // var fileId = Date.now() + '.svg';
    // var pathUpload = __dirname + '/../public/' + fileId;
    // fs.readFile(req.files.file.path, function (err, data) {
    //   if (!err) {
    //     fs.writeFile(pathUpload, data, function (err) {
    //       if (!err) {
    var request = {
      name: req.body.name,
      major: req.body.major,
      contact: req.body.contact,
      address: req.body.address,
      dateTime: req.body.dateTime,
      human: req.body.human,
      scale: req.body.scale,
      description: req.body.description,
      website: req.body.website,
      user: req.user
      // fileId: fileId
    };
    companyDao.createCompany(request)
      .then((company) => {
        console.log(company);
        res.status(200).send(company).end();
      }).catch((err) => {
        console.log(err);
        // fs.unlink(pathUpload);
        res.status(400).send(err).end();
      });
    //     }
    //   })
    // }
    // })
    // for (var f in req.files) {
    //   if (f) {
    //     fs.unlink(req.files[f].path);
    //   }
    // }

  }


  function uploadImage(req, res, next) {
    var request = {
      image: req.profileImageURL,
      user: req.user
    };
    companyDao.uploadImage(request)
      .then(function (success) {
        res.status(200).send(success).end();
      }).catch((err) => {
        res.status(404).send(err).end();
      });
  }
  /**
   * @api {get} /api/team/get/:id GetTeam
   * @apiDescription Get a single team
   * @apiVersion 0.0.1
   * @apiName  GetTeam
   * @apiGroup Team
   * @apiPermission User
   *
   * @apiExample {curl} Example usage:
   *      curl -i http://localhost/api/team/get/592ac0636f64472b6c39872d
   *
   * @apiParamExample {json} Params-Example
   *  {
   *     teamId:"592ac0636f64472b6c39872d"
   *  }
   *
   * @apiParam (Request Query) {ObjectId} teamId teamId
   *
   * @apiSuccess (Response Header 200) {String} Content-Type="application/json" Content Type
   * @apiSuccess (Response Body 200) {ObjectId} _id Team id
   * @apiSuccess (Response Body 200) {String} name name
   * @apiSuccess (Response Body 200) {String} logo logo
   * @apiSuccess (Response Body 200) {String} colours colours
   * @apiSuccess (Response Body 200) {String} gallary gallary
   *
   * @apiSuccessExample {json} Success-Response:
   * HTTP/1.1 200 GET_TEAM_SUCCESS
      {
      "message": "GET_TEAM_SUCCESS",
      "team": {
          "_id": "592ac0636f64472b6c39872d",
          "name": "FC CN3",
          "date": "01/01/2013",
          "logo": "logo.jpg",
          "colours": "red",
          "gallary": "champion cup 2013",
          "__v": 0,
          "members": [
          {
              "_id": "592ac0a86f64472b6c39872f",
              "playerNumber": 0,
              "position": "GK",
              "role": "player",
              "userId": "5923e38a6171b55d72f57f62"
          }
          ]
      }
      }}
   * @apiError (Response Body 403) {String} message Error message
   * @apiErrorExample {json} Error-403-Response:
   *  {
   *      "message": "ERROR_INPUT"
   * }
   * @apiError (Response Body 500) {String} message Error message
   * @apiErrorExample {json} Error-500-Response:
   *  {
   *      "message": "SYSTEM_ERROR"
   * }
   *   @apiError (Response Body 404) {String} message Error message
   * @apiErrorExample {json} Error-404-Response:
   *  {
   *      "message": "NOT_FOUND_TEAM"
   * }
   */
  function getCompany(req, res, next) {
    var request = {
      companyId: req.params.id
    };

    companyDao.getCompany(request)
      .then(function (company) {
        if (company) {
          res.status(200).send(company).end();
        }
      }).catch((err) => {
        res.status(err.statusCode).send(err.msg).end();
      });
  }

  /**
   * @api {put} /api/team/update UpdateTeam
   * @apiDescription UpdateTeam
   * @apiVersion 0.0.1
   * @apiName  UpdateTeam
   * @apiGroup Team
   * @apiPermission User
   *
   * @apiExample {curl} Example usage:
   *      curl -i http://localhost/api/team/update/592ac0636f64472b6c39872d
   *
   * @apiParamExample {json} Params-Example
      {
          "name":"FC CN3",
          "date":"01/01/2013",
          "logo":"logo.jpg",
          "colours":"red",
          "gallary":"champion cup 2013"
      }
   *
   * @apiParam (Request Param) {ObjectId} teamId teamId
   * @apiParam (Request Param) {String} name Name
   * @apiParam (Request Param) {String} date Date
   * @apiParam (Request Param) {String} logo Logo
   * @apiParam (Request Param) {String} colours Colours
   * @apiParam (Request Param) {String} gallary Gallary
   *
   * @apiSuccess (Response Header 200) {String} Content-Type="application/json" Content Type
   *
   * @apiSuccessExample {json} Success-Response:
   * HTTP/1.1 200 UPDATE_SUCCESS
      {
          "message": "UPDATE_TEAM_SUCCESS"
      }
   * @apiError (Response Body 403) {String} message Error message
   * @apiErrorExample {json} Error-403-Response:
   *  {
   *      "message": "ERROR_INPUT"
   * }
   * @apiError (Response Body 500) {String} message Error message
   * @apiErrorExample {json} Error-500-Response:
   *  {
   *      "message": "SYSTEM_ERROR"
   * }
   *   @apiError (Response Body 404) {String} message Error message
   * @apiErrorExample {json} Error-404-Response:
   *  {
   *      "message": "NOT_FOUND_TEAM"
   * }
   */
  function updateCompany(req, res, next) {
    var request = {
      companyId: req.params.id,
      name: req.body.name,
      contact: req.body.contact,
      address: req.body.address,
      dateTime: req.body.dateTime,
      human: req.body.human,
      scale: req.body.scale,
      description: req.body.description,
      website: req.body.website,
    };
    companyDao.updateCompany(request)
      .then(function (company) {
        if (company) {
          res.status(200).send(company).end();
        }
      }).catch((err) => {
        res.status(err.statusCode).send(err.msg).end();
      });
  }


  /**
       * @api {delete} /api/team/delete DeleteTeam
       * @apiDescription DeleteTeam
       * @apiVersion 0.0.1
       * @apiName  DeleteTeam
       * @apiGroup Team
       * @apiPermission User
       *
       * @apiExample {curl} Example usage:
       *      curl -i http://localhost/api/team/delete592ac0636f64472b6c39872d
       *
       * @apiParamExample {json} Params-Example
          {
  	        "teamId":"59268e6bdf615c196092f0db"
          }
       *
       * @apiParam (Request Param) {ObjectId} teamId teamId
       *
       * @apiSuccess (Response Header 200) {String} Content-Type="application/json" Content Type
       * @apiSuccessExample {json} Success-Response:
       * HTTP/1.1 200 UPDATE_SUCESS
       {
          "message": "REMOVE_TEAM_SUCCESS"
       }
       * @apiError (Response Body 403) {String} message Error message
       * @apiErrorExample {json} Error-403-Response:
       *  {
       *      "message": "ERROR_INPUT"
       * }
       * @apiError (Response Body 500) {String} message Error message
       * @apiErrorExample {json} Error-500-Response:
       *  {
       *      "message": "SYSTEM_ERROR"
       * }
       * @apiError (Response Body 404) {String} message Error message
       * @apiErrorExample {json} Error-404-Response:
       *  {
       *      "message": "NOT_FOUND_TEAM"
       * }
       */
  function deleteCompany(req, res, next) {
    var request = {
      companyId: req.body.companyId || req.query.companyId
    };

    companyDao.deleteCompany(request)
      .then(function (company) {
        if (company) {
          res.status(200).send(company).end();
        }
      }).catch((err) => {
        res.status(err.statusCode).send(err.msg).end();
      });
  }

  return router;
};
