/*jshint esversion: 6 */
var router = require('express').Router();
var cvDao = require('./../dao/cv.dao');
var middlewareJwt = require('../middlewares/jwt');


module.exports = function () {
    router.post('/create',middlewareJwt.authentication(["user"]), createCV);
    router.get('/get/:id', middlewareJwt.authentication(["user","company"]), getCV);
    router.put('/update/:id', updateCV);
    router.delete('/delete/:id', deleteCV);
    router.get('/checkCV', middlewareJwt.authentication(["user"]), checkCV);


    function createCV(req, res, next) {
        console.log(req.user);
        var request = {
            certificate:req.body.certificate,
            experience: req.body.experience,
            language: req.body.language,
            userId: req.user._id
        };
        cvDao.createCV(request)
            .then(function (cv) {
                res.status(200).send(cv).end();
            }).catch((err) => {
                console.log(err);
                res.status(400).send(err.msg).end();
            });
    }

    function getCV(req, res, next) {
        var request = {
            cvId: req.params.id
        };

        cvDao.getCV(request)
            .then((cv) =>{
                if (cv) {
                    console.log(cv);
                    res.status(200).send(cv).end();
                }
            }).catch((err) => {
                res.status(err.statusCode).send(err.msg).end();
            });
    }

    function checkCV(req, res, next) {
        var request = {
            userId: req.user._id
        };

        cvDao.checkCV(request)
            .then(function (cv) {
                if (cv) {
                    res.status(200).send(cv).end();
                }
            }).catch((err) => {
                console.log(err)
                res.status(err.statusCode).send(err.msg).end();
            });
    }

   function updateCV(req, res, next) {
        var request = {
            cvId: req.params.id,
            certificate: req.body.certificate,
            experience: req.body.experience,
            language: req.body.language
        };
        cvDao.updateCV(request)
            .then(function (cv) {
                if (cv) {
                    res.status(200).send(cv).end();
                }
            }).catch((err) => {
                res.status(err.statusCode).send(err.msg).end();
            });
    }


   function deleteCV(req, res, next) {
        var request = {
            cvId: req.body.cvId || req.query.cvId
        };

        cvDao.deleteCV(request)
            .then(function (cv) {
                if (cv) {
                    res.status(200).send(cv).end();
                }
            }).catch((err) => {
                res.status(err.statusCode).send(err.msg).end();
            });
    }

    return router;
};
