var router = require('express').Router();
var matchDao = require('./../dao/match.dao');
var middlewareJwt = require('../middlewares/jwt');
var moment = require('moment');

var failMessage = require('../services/failMessage');

module.exports = function () {
    router.post('/create',middlewareJwt.authentication, createMatch);
    router.post('/createChallenge',middlewareJwt.authentication, createChallenged);
    router.get('/get/:id',middlewareJwt.authentication, getMatch);
    router.put('/update/:id',middlewareJwt.authentication, updateMatch);
    router.delete('/delete/:id',middlewareJwt.authentication, deleteMatch);
    router.get('/getOngoing/:id',middlewareJwt.authentication, getMatchOngoing);
    router.get('/getPagination/:id/:pageIndex/:pageSize', getPagination);
    router.put('/acceptChallenger/:matchId',middlewareJwt.authentication,acceptChallenger);
    router.get('/getChallengerMatch/:id',middlewareJwt.authentication,getChallengerMatch);
    router.get('/getAcceptedMatch/:id',middlewareJwt.authentication,getAcceptedMatch);


    /**
    * @api {post} /api/match/create CreateMatch
    * @apiDescription CreateMatch
    * @apiVersion 0.0.1
    * @apiName  CreateMatch
    * @apiGroup Match
    * @apiPermission User
    *
    * @apiExample {curl} Example usage:
    *      curl -i http://localhost/api/match/create
    *
    * @apiParamExample {json} Params-Example
        {
            "hometeamId":"592ac0636f64472b6c39872d",
            "awayteamId":"592b96a13c1d02210048345a",
            "pitchId":"592e42fc88eddb38c87c864c",
            "datetime":"04/06/2017",
            "type": "test",
            "status":"",
            "result":""
        }
    *
    * @apiParam (Request Body) {ObjectId} hometeamId hometeamId
    * @apiParam (Request Body) {ObjectId} awayteamId awayteamId
    * @apiParam (Request Body) {ObjectId} pitchId pitchId
    * @apiParam (Request Body) {String} datetime datetime
    * @apiParam (Request Body) {String} type type
    * @apiParam (Request Body) {String} status status
    * @apiParam (Request Body) {String} result result
    *   
    * @apiSuccess (Response Header 200) {String} Content-Type="application/json" Content Type
    *
    * @apiSuccessExample {json} Success-Response:
    * HTTP/1.1 200 CREATE_SUCCESS
        {
            "message": "CREATE_MATCH_SUCCESS"
        }
    * @apiError (Response Body 403) {String} message Error message
    * @apiErrorExample {json} Error-403-Response:
    *  {
    *      "message": "ERROR_INPUT"
    * }
    * @apiError (Response Body 500) {String} message Error message
    * @apiErrorExample {json} Error-500-Response:
    *  {
    *      "message": "SYSTEM_ERROR"
    * }
    * @apiError (Response Body 404) {String} message Error message
    * @apiErrorExample {json} Error-404-Response:
    *  {
    *      "message": "NOT_FOUND"
    * }
    * @apiError (Response Body 400) {String} message Error message
    * @apiErrorExample {json} Error-400-Response:
    *  {
    *      "message": "MATCH_EXIST"
    * }
    */
    function createMatch(req, res, next) {
        var request = {
            hometeamId: req.body.hometeamId,
            awayteamId: req.body.awayteamId,
            pitchId: req.body.pitchId,
            datetime: req.body.datetime,
            type: req.body.type,
            status: req.body.status,
            result: req.body.result
        };
        matchDao.createMatch(request)
            .then(function (match) {
                res.status(200).send(match).end();
            })
            .catch(function (err) {
                console.log(err);
                res.status(err.statusCode).send(err.message).end();
            });
    }

    function createChallenged(req, res, next) {
        var request = {
            hometeamId: req.body.hometeamId,
            awayteamId: req.body.awayteamId,
            pitchId: req.body.pitchId,
            datetime: req.body.datetime,
            type: req.body.type,
            status: req.body.status,
            result: req.body.result
        };
        matchDao.createChallenged(request)
            .then(function (match) {
                res.status(200).send(match).end();
            })
            .catch(function (err) {
                console.log(err);
                res.status(err.statusCode).send(err.message).end();
            });
    }

    /**
        * @api {get} /api/match/get GetMatch
        * @apiDescription GetMatch
        * @apiVersion 0.0.1
        * @apiName  GetMatch
        * @apiGroup Match
        * @apiPermission User
        *
        * @apiExample {curl} Example usage:
        *      curl -i http://localhost/api/match/get
        *
        * @apiParamExample {json} Params-Example
            {
                "matchId":"592ac0636f64472b6c39872d"
            }
        *
        * @apiParam (Request Query) {ObjectId} matchId matchId
        *   
        * @apiSuccess (Response Header 200) {String} Content-Type="application/json" Content Type
        * @apiSuccess (Response Body 200) {ObjectId} _id Team id
        * @apiSuccess (Response Body 200) {ObjectId} hometeamId hometeamId
        * @apiSuccess (Response Body 200) {ObjectId} awayteamId awayteamId
        * @apiSuccess (Response Body 200) {ObjectId} pitchId pitchId
        * @apiSuccess (Response Body 200) {String} datetime datetime
        * @apiSuccess (Response Body 200) {String} type type
        * @apiSuccess (Response Body 200) {String} status status
        * @apiSuccess (Response Body 200) {String} result result
        *
        * @apiSuccessExample {json} Success-Response:
        * HTTP/1.1 200 GET_SUCCESS
            {
                "message": "GET_MATCH_SUCCESS",
                "match": {
                    "__v": 0,
                    "hometeamId": "592ac0636f64472b6c39872d",
                    "awayteamId": "592b96a13c1d02210048345a",
                    "pitchId": "592e42fc88eddb38c87c864c",
                    "datetime": "04/06/2017",
                    "type": "test",
                    "status": "On",
                    "result": "None",
                    "_id": "5930d73219b79c1b54e9b1d6"
                }
            }
        * @apiError (Response Body 403) {String} message Error message
        * @apiErrorExample {json} Error-403-Response:
        *  {
        *      "message": "ERROR_INPUT"
        * }
        * @apiError (Response Body 500) {String} message Error message
        * @apiErrorExample {json} Error-500-Response:
        *  {
        *      "message": "SYSTEM_ERROR"
        * }
        * @apiError (Response Body 404) {String} message Error message
        * @apiErrorExample {json} Error-404-Response:
        *  {
        *      "message": "NOT_FOUND"
        * }
        */
    function getMatch(req, res, next) {
        var request = {
            matchId: req.query.matchId
        };
        matchDao.getMatch(request, function (err, reponse) {
            if (err) {
                next(err);
            } else {
                res.status(200).send(reponse).end();
            }
        });
    }

    /**
        * @api {put} /api/match/update UpdateMatch
        * @apiDescription UpdateMatch
        * @apiVersion 0.0.1
        * @apiName  UpdateMatch
        * @apiGroup Match
        * @apiPermission User
        *
        * @apiExample {curl} Example usage:
        *      curl -i http://localhost/api/match/update
        *
        * @apiParamExample {json} Params-Example
            {
                "matchId": "5930d73219b79c1b54e9b1d6",
                "hometeamId":"592ac0636f64472b6c39872d",
                "awayteamId":"592b96a13c1d02210048345a",
                "pitchId":"592e42fc88eddb38c87c864c",
                "datetime":"04/06/2017",
                "type": "test",
                "status":"",
                "result":""
            }
        *
        * @apiParam (Request Body) {ObjectId} matchId matchId
        * @apiParam (Request Body) {ObjectId} hometeamId hometeamId
        * @apiParam (Request Body) {ObjectId} awayteamId awayteamId
        * @apiParam (Request Body) {ObjectId} pitchId pitchId
        * @apiParam (Request Body) {String} datetime datetime
        * @apiParam (Request Body) {String} type type
        * @apiParam (Request Body) {String} status status
        * @apiParam (Request Body) {String} result result
        *   
        * @apiSuccess (Response Header 200) {String} Content-Type="application/json" Content Type
        *
        * @apiSuccessExample {json} Success-Response:
        * HTTP/1.1 200 UPDATE_SUCCESS
            {
                "message": "UPDATE_MATCH_SUCCESS"
            }
        * @apiError (Response Body 403) {String} message Error message
        * @apiErrorExample {json} Error-403-Response:
        *  {
        *      "message": "ERROR_INPUT"
        * }
        * @apiError (Response Body 500) {String} message Error message
        * @apiErrorExample {json} Error-500-Response:
        *  {
        *      "message": "SYSTEM_ERROR"
        * }
        * @apiError (Response Body 404) {String} message Error message
        * @apiErrorExample {json} Error-404-Response:
        *  {
        *      "message": "NOT_FOUND"
        * }
        * @apiError (Response Body 400) {String} message Error message
        * @apiErrorExample {json} Error-400-Response:
        *  {
        *      "message": "MATCH_EXIST"
        * }
        */
    function updateMatch(req, res, next) {
        var request = {
            matchId: req.body.matchId,
            hometeamId: req.body.hometeamId,
            awayteamId: req.body.awayteamId,
            pitchId: req.body.pitchId,
            datetime: req.body.datetime,
            type: req.body.type,
            status: req.body.status,
            result: req.body.result
        };
        matchDao.updateMatch(request, function (err, reponse) {
            if (err) {
                next(err);
            } else {
                res.status(200).send(reponse).end();
            }
        });
    }

    /**
        * @api {delete} /api/match/delete DeleteMatch
        * @apiDescription DeleteMatch
        * @apiVersion 0.0.1
        * @apiName  DeleteMatch
        * @apiGroup Match
        * @apiPermission User
        *
        * @apiExample {curl} Example usage:
        *      curl -i http://localhost/api/match/delete
        *
        * @apiParamExample {json} Params-Example
            {
                "matchId":"592ac0636f64472b6c39872d"
            }
        *
        * @apiParam (Request Body) {ObjectId} matchId matchId
        *   
        * @apiSuccess (Response Header 200) {String} Content-Type="application/json" Content Type
        *
        * @apiSuccessExample {json} Success-Response:
        * HTTP/1.1 200 DELETE_SUCCESS
            {
                "message": "DELETE_MATCH_SUCCESS""
            }
        * @apiError (Response Body 403) {String} message Error message
        * @apiErrorExample {json} Error-403-Response:
        *  {
        *      "message": "ERROR_INPUT"
        * }
        * @apiError (Response Body 500) {String} message Error message
        * @apiErrorExample {json} Error-500-Response:
        *  {
        *      "message": "SYSTEM_ERROR"
        * }
        * @apiError (Response Body 404) {String} message Error message
        * @apiErrorExample {json} Error-404-Response:
        *  {
        *      "message": "NOT_FOUND"
        * }
        */
    function deleteMatch(req, res, next) {
        var request = {
            matchId: req.body.matchId
        };
        matchDao.deleteMatch(request, function (err, reponse) {
            if (err) {
                next(err);
            } else {
                res.status(200).send(reponse).end();
            }
        });
    }

    function getMatchOngoing(req, res, next) {
        var request = {
            teamId: req.params.id
            // datetime: req.body.datetime
        };
        matchDao.getMatchOngoing(request)
            .then(function (match) {
                res.status(200).send(match).end();
            })
            .catch(function (err) {
                res.status(err.statusCode).send(err.message).end();
            });
    }

    function getPagination(req, res, next) {
        var request = {
            userId: req.params.id,
            pageIndex: parseInt(req.params.pageIndex),
            pageSize: parseInt(req.params.pageSize)
        };
        matchDao.getPagination(request)
            .then(function (match) {
                res.status(200).send(match).end();
            })
            .catch(function (err) {
                res.status(err.statusCode).send(err.message).end();
            });
    }

    function getChallengerMatch(req,res,next){
        var request = {
            userId: req.params.id
        }
        matchDao.getChallengerMatch(request)
            .then(function(match){
                res.status(200).send(match).end();
            }).catch(function(err){
                res.status(err.statusCode).send(err.message).end();
            })
    }

    function getAcceptedMatch(req,res,next){
        var request = {
            userId: req.params.id
        }
        matchDao.getAcceptedMatch(request)
            .then(function(match){
                res.status(200).send(match).end();
            }).catch(function(err){
                res.status(err.statusCode).send(err.message).end();
            })
    }

    function acceptChallenger(req,res,next){
        var request = {
            matchId : req.params.matchId,
            awayteamId : req.body.awayteamId
        }
        matchDao.acceptChallenger(request)
            .then(function(match){
                console.log(match)
                res.status(match.statusCode).send(match.message).end();
            })
            .catch(function(err){
                console.log(err)
                res.status(err.statusCode).send(err.message).end();
            })
    }


    return router;
};