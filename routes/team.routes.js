/*jshint esversion: 6 */
var router = require('express').Router();
var teamDao = require('./../dao/team.dao');
var middlewareJwt = require('../middlewares/jwt');
var multipart = require('connect-multiparty');
var fs = require('fs')
var multipartMiddleware = multipart();


module.exports = function () {
  router.post('/create',middlewareJwt.authentication, multipartMiddleware, createTeam);
  router.get('/get/:id',middlewareJwt.authentication, getTeam);
  router.put('/update/:id',middlewareJwt.authentication, updateTeam);
  router.delete('/delete/:id',middlewareJwt.authentication, deleteTeam);
  router.post('/add/:id',middlewareJwt.authentication, addMemberTeam);
  router.delete('/remove/:id',middlewareJwt.authentication, removeMemberTeam);
  router.get('/getTeamRank?:limit',middlewareJwt.authentication, getTeamRank);
  router.get('/getMyTeam/:id',middlewareJwt.authentication, getMyTeam);
  router.get('/getAnotherTeam/:id',middlewareJwt.authentication, getAnotherTeam);
  router.post('/joinTeam',middlewareJwt.authentication, joinTeam);
  router.get('/joinTeam/:teamId',middlewareJwt.authentication, getJoin);
  router.put('/acceptJoinTeam',middlewareJwt.authentication, acceptJoinTeam);
  router.get('/getMessage/:id',middlewareJwt.authentication,getMessage);

  /**
   * @api {post} /api/team/create CreateTeam
   * @apiDescription CreateTeam
   * @apiVersion 0.0.1
   * @apiName  CreateTeam
   * @apiGroup Team
   * @apiPermission User
   *
   * @apiExample {curl} Example usage:
   *      curl -i http://localhost/api/team/create
   *
   * @apiParamExample {json} Params-Example
   *  {
   *   "name":"FC CN4",
   *   "date":"01/01/2013",
   *   "logo":"logo.jpg",
   *   "colours":"red",
   *   "gallary":"champion cup 2013"
   *  }
   *
   * @apiParam (Request Body) {String} name Name
   * @apiParam (Request Body) {String} date Date
   * @apiParam (Request Body) {String} logo Logo
   * @apiParam (Request Body) {String} colours Colours
   * @apiParam (Request Body) {String} gallary Gallary
   *
   * @apiSuccess (Response Header 200) {String} Content-Type="application/json" Content Type
   * @apiSuccess (Response Body 200) {String} _id Team id
   * @apiSuccess (Response Body 200) {String} name name
   * @apiSuccess (Response Body 200) {String} logo logo
   * @apiSuccess (Response Body 200) {String} colours colours
   * @apiSuccess (Response Body 200) {String} gallary gallary
   *
   * @apiSuccessExample {json} Success-Response:
   * HTTP/1.1 200 CREATE_SUCCESS
   *   {
   *       "message": "CREATE_SUCCESS",
   *       "team": {
   *           "__v": 0,
   *           "name": "FC CN2",
   *           "date": "01/01/2013",
   *           "logo": "logo.jpg",
   *           "colours": "red",
   *           "gallary": "champion cup 2013",
   *           "_id": "592b96a13c1d02210048345a",
   *           "members": []
   *       }
   *   }
   * @apiError (Response Body 403) {String} message Error message
   * @apiErrorExample {json} Error-403-Response:
   *  {
   *      "message": "ERROR_INPUT"
   * }
   * @apiError (Response Body 500) {String} message Error message
   * @apiErrorExample {json} Error-500-Response:
   *  {
   *      "message": "SYSTEM_ERROR"
   * }
   * @apiError (Response Body 400) {String} message Error message
   * @apiErrorExample {json} Error-400-Response:
   *  {
   *      "message": "DUPPLICATE_TEAM"
   * }
   * @apiError (Response Body 404) {String} message Error message
   * @apiErrorExample {json} Error-404-Response:
   *  {
   *      "message": "NOT_FOUND"
   * }
   */

  function createTeam(req, res, next) {
    // console.log(req.files.file);

    var file = req.files.file;
    var fileId = Date.now() + '.svg';
    var pathUpload = __dirname + '/../public/' + fileId;
    fs.readFile(req.files.file.path, function (err, data) {
      if (!err) {
        fs.writeFile(pathUpload, data, function (err) {
          if (!err) {
            var request = {
              name: req.body.name,
              owner: req.body.owner,
              location: req.body.location,
              fileId: fileId
            };
            console.log(request);
            teamDao.createTeam(request)
              .then(function (team) {
                res.status(200).send(team).end();
              }).catch((err) => {
                fs.unlink(pathUpload);
                res.status(err.statusCode).send(err.message).end();
              });
          }
        })
      }
    })
    for (var f in req.files) {
      if (f) {
        fs.unlink(req.files[f].path);
      }
    }

  }

  /**
   * @api {get} /api/team/get/:id GetTeam
   * @apiDescription Get a single team
   * @apiVersion 0.0.1
   * @apiName  GetTeam
   * @apiGroup Team
   * @apiPermission User
   *
   * @apiExample {curl} Example usage:
   *      curl -i http://localhost/api/team/get/592ac0636f64472b6c39872d
   *
   * @apiParamExample {json} Params-Example
   *  {
   *     teamId:"592ac0636f64472b6c39872d"
   *  }
   *
   * @apiParam (Request Query) {ObjectId} teamId teamId
   *
   * @apiSuccess (Response Header 200) {String} Content-Type="application/json" Content Type
   * @apiSuccess (Response Body 200) {ObjectId} _id Team id
   * @apiSuccess (Response Body 200) {String} name name
   * @apiSuccess (Response Body 200) {String} logo logo
   * @apiSuccess (Response Body 200) {String} colours colours
   * @apiSuccess (Response Body 200) {String} gallary gallary
   *
   * @apiSuccessExample {json} Success-Response:
   * HTTP/1.1 200 GET_TEAM_SUCCESS
      {
      "message": "GET_TEAM_SUCCESS",
      "team": {
          "_id": "592ac0636f64472b6c39872d",
          "name": "FC CN3",
          "date": "01/01/2013",
          "logo": "logo.jpg",
          "colours": "red",
          "gallary": "champion cup 2013",
          "__v": 0,
          "members": [
          {
              "_id": "592ac0a86f64472b6c39872f",
              "playerNumber": 0,
              "position": "GK",
              "role": "player",
              "userId": "5923e38a6171b55d72f57f62"
          }
          ]
      }
      }}
   * @apiError (Response Body 403) {String} message Error message
   * @apiErrorExample {json} Error-403-Response:
   *  {
   *      "message": "ERROR_INPUT"
   * }
   * @apiError (Response Body 500) {String} message Error message
   * @apiErrorExample {json} Error-500-Response:
   *  {
   *      "message": "SYSTEM_ERROR"
   * }
   *   @apiError (Response Body 404) {String} message Error message
   * @apiErrorExample {json} Error-404-Response:
   *  {
   *      "message": "NOT_FOUND_TEAM"
   * }
   */
  function getTeam(req, res, next) {
    var request = {
      teamId: req.params.id
    };

    teamDao.getTeam(request)
      .then(function (team) {
        if (team) {
          res.status(200).send(team).end();
        }
      }).catch((err) => {
        res.status(err.statusCode).send(err.message).end();
      });
  }

  /**
   * @api {put} /api/team/update UpdateTeam
   * @apiDescription UpdateTeam
   * @apiVersion 0.0.1
   * @apiName  UpdateTeam
   * @apiGroup Team
   * @apiPermission User
   *
   * @apiExample {curl} Example usage:
   *      curl -i http://localhost/api/team/update/592ac0636f64472b6c39872d
   *
   * @apiParamExample {json} Params-Example
      {
          "name":"FC CN3",
          "date":"01/01/2013",
          "logo":"logo.jpg",
          "colours":"red",
          "gallary":"champion cup 2013"
      }
   *
   * @apiParam (Request Param) {ObjectId} teamId teamId
   * @apiParam (Request Param) {String} name Name
   * @apiParam (Request Param) {String} date Date
   * @apiParam (Request Param) {String} logo Logo
   * @apiParam (Request Param) {String} colours Colours
   * @apiParam (Request Param) {String} gallary Gallary
   *
   * @apiSuccess (Response Header 200) {String} Content-Type="application/json" Content Type
   *
   * @apiSuccessExample {json} Success-Response:
   * HTTP/1.1 200 UPDATE_SUCCESS
      {
          "message": "UPDATE_TEAM_SUCCESS"
      }
   * @apiError (Response Body 403) {String} message Error message
   * @apiErrorExample {json} Error-403-Response:
   *  {
   *      "message": "ERROR_INPUT"
   * }
   * @apiError (Response Body 500) {String} message Error message
   * @apiErrorExample {json} Error-500-Response:
   *  {
   *      "message": "SYSTEM_ERROR"
   * }
   *   @apiError (Response Body 404) {String} message Error message
   * @apiErrorExample {json} Error-404-Response:
   *  {
   *      "message": "NOT_FOUND_TEAM"
   * }
   */
  function updateTeam(req, res, next) {
    var request = {
      teamId: req.params.id,
      name: req.body.name,
      date: req.body.date,
      logo: req.body.logo,
      colours: req.body.colours,
      gallary: req.body.gallary
    };
    teamDao.updateTeam(request)
      .then(function (team) {
        if (team) {
          res.status(200).send(team).end();
        }
      }).catch((err) => {
        res.status(err.statusCode).send(err.message).end();
      });
  }


  /**
       * @api {delete} /api/team/delete DeleteTeam
       * @apiDescription DeleteTeam
       * @apiVersion 0.0.1
       * @apiName  DeleteTeam
       * @apiGroup Team
       * @apiPermission User
       *
       * @apiExample {curl} Example usage:
       *      curl -i http://localhost/api/team/delete592ac0636f64472b6c39872d
       *
       * @apiParamExample {json} Params-Example
          {
  	        "teamId":"59268e6bdf615c196092f0db"
          }
       *
       * @apiParam (Request Param) {ObjectId} teamId teamId
       *
       * @apiSuccess (Response Header 200) {String} Content-Type="application/json" Content Type
       * @apiSuccessExample {json} Success-Response:
       * HTTP/1.1 200 UPDATE_SUCESS
       {
          "message": "REMOVE_TEAM_SUCCESS"
       }
       * @apiError (Response Body 403) {String} message Error message
       * @apiErrorExample {json} Error-403-Response:
       *  {
       *      "message": "ERROR_INPUT"
       * }
       * @apiError (Response Body 500) {String} message Error message
       * @apiErrorExample {json} Error-500-Response:
       *  {
       *      "message": "SYSTEM_ERROR"
       * }
       * @apiError (Response Body 404) {String} message Error message
       * @apiErrorExample {json} Error-404-Response:
       *  {
       *      "message": "NOT_FOUND_TEAM"
       * }
       */
  function deleteTeam(req, res, next) {
    var request = {
      teamId: req.body.teamId || req.query.teamId
    };

    teamDao.deleteTeam(request)
      .then(function (team) {
        if (team) {
          res.status(200).send(team).end();
        }
      }).catch((err) => {
        res.status(err.statusCode).send(err.message).end();
      });
  }


  /**
   * @api {post} /api/team/add AddMemberTeam
   * @apiDescription AddMemberTeam
   * @apiVersion 0.0.1
   * @apiName  AddMemberTeam
   * @apiGroup Team
   * @apiPermission User
   *
   * @apiExample {curl} Example usage:
   *      curl -i http://localhost/api/team/add
   *
   * @apiParamExample {json} Params-Example
      {
          "userId":"5923e38a6171b55d72f57f62",
          "role":"player",
          "position":"GK",
          "playerNumber":"0"
      }
   *
   * @apiParam (Request Param) {ObjectId} teamId teamId
   * @apiParam (Request Param) {ObjectId} userId userId
   * @apiParam (Request Param) {String} role Role
   * @apiParam (Request Param) {String} position Position
   * @apiParam (Request Param) {String} playerNumber PlayerNumber
   *
   * @apiSuccess (Response Header 200) {String} Content-Type="application/json" Content Type
   * @apiSuccessExample {json} Success-Response:
   * HTTP/1.1 200 ADD_SUCCESS
   {
      "message": "ADD_MEMBER_SUCCESS"
   }
   * @apiError (Response Body 403) {String} message Error message
   * @apiErrorExample {json} Error-403-Response:
   *  {
   *      "message": "ERROR_INPUT"
   * }
   * @apiError (Response Body 500) {String} message Error message
   * @apiErrorExample {json} Error-500-Response:
   *  {
   *      "message": "SYSTEM_ERROR"
   * }
   *   @apiError (Response Body 404) {String} message Error message
   * @apiErrorExample {json} Error-404-Response:
   *  {
   *      "message": "NOT_FOUND_TEAM"
   * }
   */
  function addMemberTeam(req, res, next) {
    var request = {
      teamId: req.params.id,
      userId: req.body.userId,
      role: req.body.role,
      position: req.body.position,
      playerNumber: req.body.playerNumber
    };
    teamDao.addMemberTeam(request)
      .then(function (team) {
        if (team) {
          res.status(200).send(team).end();
        }
      }).catch((err) => {
        res.status(err.statusCode).send(err.message).end();
      });
  }

  /**
      * @api {delete} /api/team/remove RemoveMemberTeam
      * @apiDescription RemoveMemberTeam
      * @apiVersion 0.0.1
      * @apiName  RemoveMemberTeam
      * @apiGroup Team
      * @apiPermission User
      *
      * @apiExample {curl} Example usage:
      *      curl -i http://localhost/api/team/remove
      *
      * @apiParamExample {json} Params-Example
         {
             "teamId":"592b8f366c62f51c5078a8a0",
             "userId":"5923e38a6171b55d72f57f62"
         }
      *
      * @apiParam (Request Param) {ObjectId} teamId teamId
      * @apiParam (Request Param) {ObjectId} userId userId
      *
      * @apiSuccess (Response Header 200) {String} Content-Type="application/json" Content Type
      * @apiSuccessExample {json} Success-Response:
      * HTTP/1.1 200 REMOVE_SUCCESS
      {
         "message": "REMOVE_MEMBER_SUCCESS"
      }
      * @apiError (Response Body 403) {String} message Error message
      * @apiErrorExample {json} Error-403-Response:
      *  {
      *      "message": "ERROR_INPUT"
      * }
      * @apiError (Response Body 500) {String} message Error message
      * @apiErrorExample {json} Error-500-Response:
      * {
      *      "message": "SYSTEM_ERROR"
      * }
      *   @apiError (Response Body 404) {String} message Error message
      * @apiErrorExample {json} Error-404-Response:
      * {
      *      "message": "USER_NOT_FOUND"
      * }
      */
  function removeMemberTeam(req, res, next) {
    var request = {
      teamId: req.params.id,
      userId: req.body.userId
    };
    teamDao.removeMemberTeam(request)
      .then(function (team) {
        if (team) {
          res.status(200).send(team).end();
        }
      }).catch((err) => {
        res.status(err.statusCode).send(err.message).end();
      });
  }

  function getTeamRank(req, res, next) {
    var request = {
      limit: req.query.limit
    }
    teamDao.getTeamRank(request).then(function (team) {
      if (team) {
        res.status(200).send(team).end();
      }
    }).catch((err) => {
      res.status(err.statusCode).send(err.message).end();
    })
  }

  function getMyTeam(req, res, next) {
    var request = {
      id: req.params.id
    }
    teamDao.getMyTeam(request).then(function (team) {
      if (team) {
        res.status(200).send(team).end();
      }
    }).catch((err) => {
      res.status(err.statusCode).send(err.message).end();
    })
  }

  function getMessage(req, res, next) {
    var request = {
      id: req.params.id
    }
    teamDao.getMessage(request).then(function (listMessage) {
      if (listMessage) {
        res.status(200).send(listMessage).end();
      }
    }).catch((err) => {
      res.status(err.statusCode).send(err.message).end();
    })
  }

  function getAnotherTeam(req, res, next) {
    var request = {
      id: req.params.id
    }
    teamDao.getAnotherTeam(request).then(function (team) {
      if (team) {
        res.status(200).send(team).end();
      }
    }).catch((err) => {
      res.status(err.statusCode).send(err.message).end();
    })
  }

  function joinTeam(req, res, next) {
    var request = {
      userId: req.body.userId,
      teamId: req.body.teamId
    }
    teamDao.joinTeam(request).then(function (requestJoin) {
      if (requestJoin) {
        console.log(requestJoin);
        res.status(200).send(requestJoin.message).end();
      }
    }).catch((err) => {
      res.status(err.statusCode).send(err.message).end();
    })
  }

  function getJoin(req, res, next) {
    var request = {
      teamId: req.params.teamId
    }
    teamDao.getJoin(request).then(function (response) {
      if (requestJoin) {
        res.status(200).send(response.team).end();
      }
    }).catch((err) => {
      res.status(err.statusCode).send(err.message).end();
    })
  }

  function acceptJoinTeam(req, res, next) {
    var request = {
      requestId: req.body.requestId,
      teamId: req.body.teamId,
      userId: req.body.userId,
      role: req.body.role,
      position: req.body.position,
      playerNumber: req.body.playerNumber
    }
    teamDao.acceptJoinTeam(request)
      .then(function (team) {
        if (team) {
          res.status(200).send(team).end();
        }
      }).catch((err) => {
        res.status(err.statusCode).send(err.message).end();
      });
  }

  return router;
};
