/*jshint esversion: 6 */
var router = require('express').Router();
var pitchDao = require('./../dao/pitch.dao');
var failMessage = require('../services/failMessage');

module.exports = function () {
    router.post('/create', createPitch);
    router.get('/',getAllPitch);
    router.get('/get/:pitchId', getPitch);
    router.put('/update', updatePitch);
    router.delete('/delete', deletePitch);
    // router.post('/addPrice', addPriceList);
    router.post('/addFeedback', addFeedback);
    router.put('/updateFeedback', updateFeedback);
    router.delete('/removeFeedback', removeFeedback);
    router.get('/getPitchDistance/:teamId',getPitchDistance);

    /**
     * @api {post} /api/pitch/create CreatePitch
     * @apiDescription CreatePitch
     * @apiVersion 0.0.1
     * @apiName  CreatePitch
     * @apiGroup Pitch
     * @apiPermission User
     *
     * @apiExample {curl} Example usage:
     *      curl -i http://localhost/api/pitch/create
     *
     * @apiParamExample {json} Params-Example
        {
            "name":"Dai Chau 2",
            "image":"daichau.jpg",
            "address":"109 Man Thien",
            "hotline":"0911000222",
            "quanlity":"7",
            "description":"BLA BLA BLA BLA"
        }
     *
     * @apiParam (Request Body) {String} name Name
     * @apiParam (Request Body) {String} image Image
     * @apiParam (Request Body) {String} address Adress
     * @apiParam (Request Body) {String} hotline Hotline
     * @apiParam (Request Body) {String} quanlity quanlity
     * @apiParam (Request Body) {String} description Description
     *
     * @apiSuccess (Response Header 200) {String} Content-Type="application/json" Content Type
     * @apiSuccess (Response Body 200) {ObjectId} _id Team id
     * @apiSuccess (Response Body 200) {String} name name
     * @apiSuccess (Response Body 200) {String} image Image
     * @apiSuccess (Response Body 200) {String} address Address
     * @apiSuccess (Response Body 200) {String} hotline Hotline
     * @apiSuccess (Response Body 200) {String} quanlity Quanlity
     * @apiSuccess (Response Body 200) {String} description Description
     *
     * @apiSuccessExample {json} Success-Response:
     * HTTP/1.1 200 CREATE_SUCCESS
        {
            "message": "CREATE_PITCH_SUCCESS",
            "pitch": {
                "__v": 0,
                "name": "Dai Chau 2",
                "image": "daichau.jpg",
                "address": "109 Man Thien",
                "hotline": "0911000222",
                "quanlity": 7,
                "description": "BLA BLA BLA BL",
                "_id": "592fb77e4d7b803ff4c0c894",
                "feedback": [],
                "priceList": []
            }
        }
     * @apiError (Response Body 403) {String} message Error message
     * @apiErrorExample {json} Error-403-Response:
     *  {
     *      "message": "ERROR_INPUT"
     * }
     * @apiError (Response Body 500) {String} message Error message
     * @apiErrorExample {json} Error-500-Response:
     *  {
     *      "message": "SYSTEM_ERROR"
     * }
      * @apiError (Response Body 400) {String} message Error message
     * @apiErrorExample {json} Error-400-Response:
     *  {
     *      "message": "DUPLICATE_PITCH"
     * }
     */
    function createPitch(req, res, next) {
        var request = {
            name: req.body.name,
            image: req.body.image,
            address: req.body.address,
            hotline: req.body.hotline,
            quanlity: req.body.quanlity,
            description: req.body.description
        };
        if (request.name === '' || request.image === '' || request.address === '' ||
            request.hotline === '' || request.quanlity === '' || request.description === '') {
            res.status(403).send(failMessage.pitch.create.input).end();
        }
        pitchDao.createPitch(request)
            .then(function (pitch) {
                res.status(200).send(pitch).end();
            })
            .catch((err) => {
                res.status(err.statusCode).send(err.message).end();
            });
    }

    function getAllPitch(req, res, next) {
        pitchDao.getAllPitch()
            .then((pitch) => {
                res.status(200).send(pitch).end();
            })
            .catch((err) => {
                res.status(404).send(failMessage.pitch.get.notFound).end();
            });
    }


    /**
     * @api {get} /api/pit/get GetPitch
     * @apiDescription GetPitch
     * @apiVersion 0.0.1
     * @apiName  GetPitch
     * @apiGroup Pitch
     * @apiPermission User
     *
     * @apiExample {curl} Example usage:
     *      curl -i http://localhost/api/pitch/get
     *
     * @apiParamExample {json} Params-Example
        {
            pitchId: 592fb77e4d7b803ff4c0c894
        }
     *
     * @apiParam (Request Query) {ObjectId} pitchId PitchId
     *
     * @apiSuccess (Response Header 200) {String} Content-Type="application/json" Content Type
     * @apiSuccess (Response Body 200) {ObjectId} _id Team id
     * @apiSuccess (Response Body 200) {String} name name
     * @apiSuccess (Response Body 200) {String} image Image
     * @apiSuccess (Response Body 200) {String} address Address
     * @apiSuccess (Response Body 200) {String} hotline Hotline
     * @apiSuccess (Response Body 200) {String} quanlity Quanlity
     * @apiSuccess (Response Body 200) {String} description Description
     *
     * @apiSuccessExample {json} Success-Response:
     * HTTP/1.1 200 GET_SUCCESS
        {
            "message": "GET_PITCH_SUCCESS",
            "pitch": {
                "__v": 0,
                "name": "Dai Chau 2",
                "image": "daichau.jpg",
                "address": "109 Man Thien",
                "hotline": "0911000222",
                "quanlity": 7,
                "description": "BLA BLA BLA BL",
                "_id": "592fb77e4d7b803ff4c0c894",
                "feedback": [],
                "priceList": []
            }
        }
     * @apiError (Response Body 403) {String} message Error message
     * @apiErrorExample {json} Error-403-Response:
     *  {
     *      "message": "ERROR_INPUT"
     * }
     * @apiError (Response Body 500) {String} message Error message
     * @apiErrorExample {json} Error-500-Response:
     *  {
     *      "message": "SYSTEM_ERROR"
     * }
     * @apiError (Response Body 404) {String} message Error message
     * @apiErrorExample {json} Error-404-Response:
     *  {
     *      "message": "NOT_FOUND"
     * }
     */
    function getPitch(req, res, next) {
        var request = {
            pitchId: req.params.pitchId
        };
        if (request.pitchId === '') {
            res.status(403).send(failMessage.pitch.create.input);
        }
        pitchDao.getPitch(request)
            .then((pitch) => {
                res.status(200).send(pitch).end();
            })
            .catch((err) => {
                res.status(404).send(failMessage.pitch.get.notFound).end();
            });
    }

    /**
     * @api {put} /api/pitch/update UpdatePitch
     * @apiDescription UpdatePitch
     * @apiVersion 0.0.1
     * @apiName  UpdatePitch
     * @apiGroup Pitch
     * @apiPermission User
     *
     * @apiExample {curl} Example usage:
     *      curl -i http://localhost/api/pitch/update
     *
     * @apiParamExample {json} Params-Example
        {
            "pitchId":"592fb77e4d7b803ff4c0c894"
            "name":"Dai Chau 2",
            "image":"daichau.jpg",
            "address":"109 Man Thien",
            "hotline":"0911000222",
            "quanlity":"7",
            "description":"BLA BLA BLA BLA"
        }
     *
     * @apiParam (Request Body) {String} name Name
     * @apiParam (Request Body) {String} image Image
     * @apiParam (Request Body) {String} address Adress
     * @apiParam (Request Body) {String} hotline Hotline
     * @apiParam (Request Body) {String} quanlity quanlity
     * @apiParam (Request Body) {String} description Description
     *
     * @apiSuccess (Response Header 200) {String} Content-Type="application/json" Content Type
     * @apiSuccess (Response Body 200) {ObjectId} _id Team id
     * @apiSuccess (Response Body 200) {String} name name
     * @apiSuccess (Response Body 200) {String} image Image
     * @apiSuccess (Response Body 200) {String} address Address
     * @apiSuccess (Response Body 200) {String} hotline Hotline
     * @apiSuccess (Response Body 200) {String} quanlity Quanlity
     * @apiSuccess (Response Body 200) {String} description Description
     *
     * @apiSuccessExample {json} Success-Response:
     * HTTP/1.1 200 UPDATE_SUCCESS
        {
            "message": "UPDATE_PITCH_SUCCESS",
            "pitch": {
                "__v": 0,
                "name": "Dai Chau 2",
                "image": "daichau.jpg",
                "address": "109 Man Thien",
                "hotline": "0911000222",
                "quanlity": 7,
                "description": "BLA BLA BLA BL",
                "_id": "592fb77e4d7b803ff4c0c894",
                "feedback": [],
                "priceList": []
            }
        }
     * @apiError (Response Body 403) {String} message Error message
     * @apiErrorExample {json} Error-403-Response:
     *  {
     *      "message": "ERROR_INPUT"
     * }
     * @apiError (Response Body 500) {String} message Error message
     * @apiErrorExample {json} Error-500-Response:
     *  {
     *      "message": "SYSTEM_ERROR"
     *  }
     * @apiError (Response Body 404) {String} message Error message
     * @apiErrorExample {json} Error-404-Response:
     *  {
     *      "message": "NOT_FOUND"
     *  }
     * @apiError (Response Body 400) {String} message Error message
     * @apiErrorExample {json} Error-400-Response:
     *  {
     *      "message": "DUPLICATE_PITCH"
     * }
     */
    function updatePitch(req, res, next) {
        var request = {
            pitchId: req.body.pitchId,
            name: req.body.name,
            image: req.body.image,
            address: req.body.address,
            hotline: req.body.hotline,
            quanlity: req.body.quanlity,
            description: req.body.description
        };
        if (request.pitchId === '' && request.name === '') {
            res.status(403).send(failMessage.pitch.create.input).end();
        }
        pitchDao.updatePitch(request)
            .then((pitch) => {
                res.status(200).send(pitch).end();
            }).catch((err) => {
                res.status((err.statusCode) ? (err.statusCode) : 404)
                    .send((err.msg) ? (err.msg) : (failMessage.pitch.get.notFound)).end();
            });

    }


    /**
     * @api {delete} /api/pitch/delete DeletePitch
     * @apiDescription DeletePitch
     * @apiVersion 0.0.1
     * @apiName  DeletePitch
     * @apiGroup Pitch
     * @apiPermission User
     *
     * @apiExample {curl} Example usage:
     *      curl -i http://localhost/api/pitch/delete
     *
     * @apiParamExample {json} Params-Example
        {
            "pitchId":"592cdf16ec4308468cbc6c87"
        }
     *
     * @apiParam (Request Body) {ObjectId} pitchId PitchId
     *
     * @apiSuccess (Response Header 200) {String} Content-Type="application/json" Content Type
     *
     * @apiSuccessExample {json} Success-Response:
     * HTTP/1.1 200 DELETE_SUCCESS
        {
            "message": "DELETE_PITCH_SUCCESS",
        }
     * @apiError (Response Body 403) {String} message Error message
     * @apiErrorExample {json} Error-403-Response:
     *  {
     *      "message": "ERROR_INPUT"
     * }
     * @apiError (Response Body 500) {String} message Error message
     * @apiErrorExample {json} Error-500-Response:
     *  {
     *      "message": "SYSTEM_ERROR"
     * }
     * @apiError (Response Body 404) {String} message Error message
     * @apiErrorExample {json} Error-404-Response:
     *  {
     *      "message": "NOT_FOUND"
     * }
     */
    
    
    
    
    // function deletePitch(req, res, next) {
    //     var request = {
    //         pitchId: req.body.pitchId || req.query.pitchId
    //     };
    //     pitchDao.deletePitch(request, function (err, reponse) {
    //         if (err) {
    //             next(err);
    //         } else {
    //             res.status(200).send(reponse).end();
    //         }
    //     });
    // }

    function deletePitch(req, res, next) {
        request = {
        pitchId: req.body.pitchId || req.query.pitchId
        }
        if (request.pitchId === "") {
        res.status(404).send(failMessage.pitch.create.input).end();
        }

        pitchDao.deletePitch(request).then(()=>{
        res.status(200).send(successMessage.pitch.delete)
        })
        .catch((err)=>{
        res.status(500).send(failMessage.pitch.create.systemErr)
        })
        
    }

    /**
     * @api {post} /api/team/addFeedback AddFeedBack
     * @apiDescription AddFeedBack
     * @apiVersion 0.0.1
     * @apiName  AddFeedBack
     * @apiGroup Pitch
     * @apiPermission User
     *
     * @apiExample {curl} Example usage:
     *      curl -i http://localhost/api/pitch/addFeedback
     *
     * @apiParamExample {json} Params-Example
        {
            "pitchId":"592e42fc88eddb38c87c864c",
            "userId":"5923e38a6171b55d72f57f62",
            "vote":"5",
            "comment":"CMT1"
        }
     *
     * @apiParam (Request Body) {ObjectId} pitchId PitchId
     * @apiParam (Request Body) {ObjectId} userId UserId
     * @apiParam (Request Body) {String} vote Vote
     * @apiParam (Request Body) {String} comment Comment
     *
     * @apiSuccess (Response Header 200) {String} Content-Type="application/json" Content Type
     *
     * @apiSuccessExample {json} Success-Response:
     * HTTP/1.1 200 ADD_SUCCESS
     *  {
     *     "message": "CREATE_PITCH_SUCCESS"
     *  }
     * @apiError (Response Body 403) {String} message Error message
     * @apiErrorExample {json} Error-403-Response:
     *  {
     *      "message": "ERROR_INPUT"
     * }
     * @apiError (Response Body 500) {String} message Error message
     * @apiErrorExample {json} Error-500-Response:
     *  {
     *      "message": "SYSTEM_ERROR"
     * }
     * @apiError (Response Body 400) {String} message Error message
     * @apiErrorExample {json} Error-400-Response:
     *  {
     *      "message": "PITCH_NOT_FOUND"
     * }
     * @apiError (Response Body 404) {String} message Error message
     * @apiErrorExample {json} Error-404-Response:
     *  {
     *      "message": "USER_NOT_FOUND"
     * }
     * @apiError (Response Body 400) {String} message Error message
     * @apiErrorExample {json} Error-400-Response:
     *  {
     *      "message": "DUPLICATE_FEEDBACK"
     * }
     */
    function addFeedback(req, res, next) {
        var request = {
            pitchId: req.body.pitchId,
            userId: req.body.userId,
            vote: req.body.vote,
            comment: req.body.comment
        };
         if (request.pitchId === '' || request.userId === '' || request.vote === '' || request.comment === '') {
            res.status(403).send(failMessage.pitch.create.input).end();
         }

        pitchDao.addFeedback(request) 
        .then((reponse) => {
            console.log("reponse: " + reponse);
            res.status(200).send(reponse).end();
        })
        .catch((err) =>{
            console.log("err: " + err);
            next(err);
        });
    }


    /**
     * @api {put} /api/pitch/updateFeedback UpdateFeedback
     * @apiDescription UpdateFeedback
     * @apiVersion 0.0.1
     * @apiName UpdateFeedback
     * @apiGroup Pitch
     * @apiPermission User
     *
     * @apiExample {curl} Example usage:
     *      curl -i http://localhost/api/pitch/updateFeedback
     *
     * @apiParamExample {json} Params-Example
        {
            "pitchId":"592e42fc88eddb38c87c864c",
            "userId":"5923e31710ca9121b49f31a8",
            "vote":"5",
            "comment":"CMT111"
        }
     *
     * @apiParam (Request Body) {ObjectId} oitchId PitchId
     * @apiParam (Request Body) {ObjectId} userId UserId
     * @apiParam (Request Body) {String} vote Vote
     * @apiParam (Request Body) {String} comment Comment
     *
     * @apiSuccess (Response Header 200) {String} Content-Type="application/json" Content Type
     *
     * @apiSuccessExample {json} Success-Response:
     * HTTP/1.1 200 UPDATE_SUCCESS
      *  {
      *      "message": "UPDATE_FEEDBACK_SUCCESS"
      *  }
     * @apiError (Response Body 403) {String} message Error message
     * @apiErrorExample {json} Error-403-Response:
     *  {
     *      "message": "ERROR_INPUT"
     * }
     * @apiError (Response Body 500) {String} message Error message
     * @apiErrorExample {json} Error-500-Response:
     *  {
     *      "message": "SYSTEM_ERROR"
     * }
     * @apiError (Response Body 404) {String} message Error message
     * @apiErrorExample {json} Error-404-Response:
     *  {
     *      "message": "PITCH_NOT_FOUND"
     * }
     * @apiError (Response Body 404) {String} message Error message
     * @apiErrorExample {json} Error-404-Response:
     *  {
     *      "message": "USER_NOT_FOUND"
     * }
    * @apiError (Response Body 404) {String} message Error message
     * @apiErrorExample {json} Error-404-Response:
     *  {
     *      "message": "NOT_FOUND"
     * }
     */
    function updateFeedback(req, res, next) {
        var request = {
            pitchId: req.body.pitchId,
            userId: req.body.userId,
            vote: req.body.vote,
            comment: req.body.comment
        };

        if (request.pitchId === '' || request.userId === '' || request.vote === '' || request.comment === '') {
            res.status(403).send(failMessage.pitch.create.input).end();
        }

        pitchDao.updateFeedback(request) 
        .then((reponse) => {
            res.status(200).send(reponse).end();
        })
        .catch((err) => {
            next(err);
        })
    }

    /**
     * @api {delete} /api/pitch/removeFeedback RemoveFeedback
     * @apiDescription RemoveFeedback
     * @apiVersion 0.0.1
     * @apiName  RemoveFeedback
     * @apiGroup Pitch
     * @apiPermission User
     *
     * @apiExample {curl} Example usage:
     *      curl -i http://localhost/api/pitch/removeFeedback
     *
     * @apiParamExample {json} Params-Example
        {
            "pitchId":"592e42fc88eddb38c87c864c",
            "userId":"5923e31710ca9121b49f31a8"
        }
     *
      * @apiParam (Request Body) {ObjectId} oitchId PitchId
     * @apiParam (Request Body) {ObjectId} userId UserId
     * @apiParam (Request Body) {String} vote Vote
     * @apiParam (Request Body) {String} comment Comment
     *
     * @apiSuccess (Response Header 200) {String} Content-Type="application/json" Content Type
     *
     * @apiSuccessExample {json} Success-Response:
     * HTTP/1.1 200 REMOVE_SUCCESS
     *  {
     *     "message": "REMOVE_FEEDBACK_SUCCESS"
     *  }
     * @apiError (Response Body 403) {String} message Error message
     * @apiErrorExample {json} Error-403-Response:
     *  {
     *      "message": "ERROR_INPUT"
     * }
     * @apiError (Response Body 500) {String} message Error message
     * @apiErrorExample {json} Error-500-Response:
     *  {
     *      "message": "SYSTEM_ERROR"
     * }
     * @apiError (Response Body 404) {String} message Error message
     * @apiErrorExample {json} Error-404-Response:
     *  {
     *      "message": "PITCH_NOT_FOUND"
     * }
     * @apiError (Response Body 404) {String} message Error message
     * @apiErrorExample {json} Error-400-Response:
     *  {
     *      "message": "USER_NOT_FOUND"
     * }
    * @apiError (Response Body 404) {String} message Error message
     * @apiErrorExample {json} Error-404-Response:
     *  {
     *      "message": "NOT_FOUND"
     * }
     */
    function removeFeedback(req, res, next) {
        var request = {
            pitchId: req.body.pitchId,
            userId: req.body.userId
        };

         if (request.pitchId === '' || request.userId === '') {
             res.status(403).send(failMessage.pitch.create.input).end();
         }

        pitchDao.removeFeedback(request) 
        .then((err) => {
            if (err) {
                next(err);
            }
        })
        .catch((reponse) => {
            res.status(200).send(reponse).end();
        })
    }

    function getPitchDistance(req, res, next) {
        var request = {
            teamId: req.params.teamId
        };
        pitchDao.getPitchDistance(request)
            .then((pitch) => {
                res.status(200).send(pitch).end();
            })
            .catch((err) => {
                res.status(404).send(failMessage.pitch.get.notFound).end();
            });
    }

    return router;
};