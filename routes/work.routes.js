/*jshint esversion: 6 */
var router = require('express').Router();
var workDao = require('./../dao/work.dao');
var middlewareJwt = require('../middlewares/jwt');


module.exports = function () {
    router.post('/create',middlewareJwt.authentication(["company"]), createWork);
    router.get('/get/:id',middlewareJwt.authentication(["user"]), getWork);
    router.get('/search', middlewareJwt.authentication(["user"]) ,searchWork);
    router.put('/update/:id', updateWork);
    router.delete('/delete/:id', deleteWork);
    router.put('/addFavorite/:id',middlewareJwt.authentication(["user"]), addFavorite);
    router.get('/getFavorite',middlewareJwt.authentication(["user"]), getFavorite);
    router.put('/sendCV/:workId', middlewareJwt.authentication(["user"]), sendCV);


    function createWork(req, res, next) {

        var request = {
            name: req.body.name,
            dateTime: req.body.dateTime,
            position: req.body.position,
            salary: req.body.salary,
            requirement: req.body.requirement,
            benifit: req.body.benifit,
            major: req.body.major,
            description: req.body.description,
            user: req.user
        };
        workDao.createWork(request)
            .then(function (work) {
                res.status(200).send(work).end();
            }).catch((err) => {
                res.status(500).send(err.message).end();
            });
    }

    function getWork(req, res, next) {
        var request = {
            workId: req.params.id
        };

        workDao.getWork(request)
            .then(function (work) {
                if (work) {
                    res.status(200).send(work).end();
                }
            }).catch((err) => {
                res.status(err.statusCode).send(err.message).end();
            });
    }

    function getFavorite(req, res, next) {
        var request = {
            userId: req.user._id
        };

        workDao.getFavorite(request)
            .then(function (work) {
                if (work) {
                    res.status(200).send(work).end();
                }
            }).catch((err) => {
                res.status(err.statusCode).send(err.msg).end();
            });
    }

    function addFavorite(req,res,next){
        var request = {
            userId: req.user._id,
            workId: req.params.id
        }
        workDao.addFavorite(request).then((msg)=>{
            res.status(200).send({msg:'Update success'}).end();
        }).catch((err)=>{
            res.status(err.statusCode).send(err.msg).end();
        })
    }

    function sendCV(req,res,next) {
        var request = {
            userId: req.user._id,
            workId: req.params.workId
        }
        workDao.sendCV(request).then((msg)=>{
            res.status(200).send({msg:'Update success'}).end();
        }).catch((err)=>{
            res.status(err.statusCode).send(err.msg).end();
        })
    }

    function searchWork(req, res, next) {
        var request = {
            search: req.query.searchText,
            searchBy: req.query.searchBy,
            user: req.user
        };

        workDao.searchWork(request)
            .then(function (work) {
                // console.log(work);
                if (work) {
                    res.status(200).send({works:work}).end();
                }
            }).catch((err) => {
                res.status(err.statusCode).send(err).end();
            });
    }

    function updateWork(req, res, next) {
        var request = {
            workId: req.params.id,
            name: req.body.name,
            dateTime: req.body.dateTime,
            position: req.body.position,
            salary: req.body.salary,
            requirement: req.body.requirement,
            benifit: req.body.benifit,
            major: req.body.major
        };
        workDao.updateWork(request)
            .then(function (work) {
                if (work) {
                    res.status(200).send(work).end();
                }
            }).catch((err) => {
                res.status(err.statusCode).send(err.message).end();
            });
    }


   function deleteWork(req, res, next) {
        var request = {
            workId: req.body.workId || req.query.workId
        };

        workDao.deleteWork(request)
            .then(function (work) {
                if (work) {
                    res.status(200).send(work).end();
                }
            }).catch((err) => {
                res.status(err.statusCode).send(err.message).end();
            });
    }

    return router;
};
