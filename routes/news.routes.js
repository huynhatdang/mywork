var router = require('express').Router();
var newsDao = require('./../dao/news.dao');
var uploadSingle = require('../middlewares/uploadSingle');

module.exports = function () {
  router.post('/',createNews);
  router.get('/', getAllNews);
  router.get('/:categoryId',getNewsByCategory);
  router.put('/like/:newId',likeNews);
  router.post('/comment',postComment);
  router.get('/getNewsById/:id', getNewsById);
//  router.put('/:id', updateNews);
  router.delete('/:id', deleteNews);

  function createNews(req, res, next) {
    var request = {
      title: req.body.title,
      description: req.body.description,
      content: req.body.content,
      // image: req.profileImageURL,
      image: req.body.image,
      author: req.body.author,
      categoryId: req.body.categoryId
    }
    newsDao.createNews(request)
      .then(function (news) {
        res.status(200).send(news).end();
      })
      .catch(function (err) {
        res.status(400).send(err).end();
      });
  }

  function getNewsByCategory(req,res,next){
    var request={
      categoryId : req.params.categoryId
    }
    newsDao.getNewsByCategory(request)
      .then(function (news) {
          res.status(200).send(news).end()
        })
        .catch(function (err) {
          res.status(400).send(err).end();
        })
  }

  function likeNews(req,res,next){
    var request ={
      newId : req.params.newId
    }
    newsDao.likeNews(request)
      .then((news)=>{
        res.status(200).send(news).end();
      })
      .catch((err)=>{
        res.status(err.status).send(err.message).end();
      })
  }

  function postComment(req,res,next){
    console.log(req.body);
    var request ={
      newId: req.body.newId,
      userId: req.body.userId,
      comment: req.body.comment
    }
    newsDao.postComment(request)
      .then((mess)=>{
        res.status(200).send(mess).end();
      })
      .catch((err)=>{
        res.status(400).send(err.message).end();
      })
  }

  function getAllNews(req, res, next) {
    newsDao.getAllNews()
      .then(function (news) {
        res.status(200).send(news).end();
      })
      .catch(function (err) {
        res.status(400).send(err).end();
      });
  }

  function getNewsById(req, res, next) {
    console.log(req.params);
    var request = {
      id: req.params.id
    }
    newsDao.getNewsById(request)
      .then(function (news) {
        res.status(200).send(news).end()
      })
      .catch(function (err) {
        res.status(400).send(err).end();
      })
  }

  function updateNews(req, res, next) {
    var request = {
      id: req.params.id,
      title: req.body.title,
      description: req.body.description,
      content: req.body.content,
      image: req.body.image,
      author: req.body.author,
      originalLink: req.body.originaLink,
      spiderId: req.body.spiderId,
      categoryId: req.body.categoryId,
    }
    newsDao.updateNews(request)
      .then(function (news) {
        res.status(200).send(news).end();
      })
      .catch(function (err) {
        res.status(400).send(err).end();
      })
  }

  function deleteNews(req, res, next) {
    var request = {
      id: req.params.id
    }
    newsDao.deleteNews(request)
      .then(function (news) {
        res.status(200).send(news).end();
      })
      .catch(function (err) {
        res.status(400).send(err).end();
      });
  }


  return router;
}