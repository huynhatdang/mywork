var News = require('./../models/news.model');
var successMessage = require('./../services/successMessage');
var failMessage = require('./../services/failMessage');
var fs= require('fs');

module.exports = {
  createNews: createNews,
  getAllNews: getAllNews,
   getNewsById: getNewsById,
   getNewsByCategory: getNewsByCategory,
   likeNews: likeNews,
   postComment: postComment,
  // updateNews: updateNews,
   deleteNews: deleteNews,
   addNews: addNews
};

function addNews(request) {
  var news = new News({
    title: request.title,
    description: request.description,
    content: request.content,
    image: request.image,
    author: request.author,
    categoryId: request.categoryId,
    createDate: request.createDate,
    updateDate: Date.now()
  });
  console.log(news.title + " jump to dao news");
  News.findOne({
    title: request.title
  }).exec().then(function (New) {
    if (New !== null) {
      console.log('news log');
      return;
    }
    news.save();
  });
}

function createNews(request) {
  return News.findOne({
    title: request.title
  }).exec().then(function (New) {
    if (New !== null) {
      return Promise.reject({
        message: failMessage.news.dupplicate
      });
    }
    var pathImage="./public/"+request.title+'.jpg';
    fs.writeFile(pathImage, request.image , 'base64', function(err) {
      if(err) {
        console.log(err);
      };
      console.log("thanh cong!!!")
    });
    var news = new News({
    title: request.title,
    description: request.description,
    content: request.content,
    image: '/public/'+request.title+'.jpg',
    like: 1,
    author: request.author,
    categoryId: request.categoryId,
    createDate: Date.now(),
    updateDate: Date.now()
  });
    return news.save().then(function (err) {
      return Promise.resolve({
        message: successMessage.news.create
      });
    });
  });
}

function getAllNews() {
  return News.find().sort({createDate:-1}).exec()
    .then(function (newss) {
      if (newss.length === 0) {
        return Promise.reject({
          message: failMessage.news.notFound
        });
      }
      return Promise.resolve({
        message: successMessage.news.getAll,
        news: newss
      });
    });
}

function likeNews(request){
  console.log(request);
  return News.findOne({
    _id: request.newId
  }).exec().then((news)=>{
    console.log(news);
    if(news === null){
      return Promise.reject({
        status: 403,
        message: failMessage.news.notFound
      });
    }
    news.like+=1;
    return news.save().then((err)=>{
      return Promise.resolve({
        message: successMessage.news.update,
        news: news
      })
    })
  })
}

function postComment(request){
  return News.findOne({
    _id: request.newId
  }).exec().then((news)=>{
    if(news===null){
      return Promise.reject({
        status: 403,
        message: failMessage.news.notFound
      })
    }
    var newComment= {
      userId: request.userId,
      comment: request.comment
    }
    return News.update({_id:request.newId},{$push: {comment: newComment}})
      .then(()=>{
        return Promise.resolve({
          message: successMessage.news.update
        })
      .catch((err)=>{
        return Promise.reject({
          message: failMessage.news.notFound
        })
      })
      })
  })
}


function getNewsById(request) {
  console.log(request);
  return News.findOne({
    _id: request.id
  }).exec().then(function (news) {
    if (news === null) {
      return Promise.reject({
        message: failMessage.news.notFound
      });
    }
    return Promise.resolve({
      message: successMessage.news.get,
      news: news
    });
  });
}

function getNewsByCategory(request){
  return News.find({categoryId:request.categoryId}).exec()
    .then(function (news) {
    if (news === null) {
      return Promise.reject({
        message: failMessage.news.notFound
      });
    }
    return Promise.resolve({
      message: successMessage.news.get,
      news: news
    });
  });
}

function deleteNews(request) {
  return News.findByIdAndRemove({
    _id: request.id
  }).exec().then(function (err) {
    if (!err) {
      return Promise.reject({
        message: failMessage.news.notFound
      });
    }
    return Promise.resolve({
      message: successMessage.news.delete
    });
  });
}