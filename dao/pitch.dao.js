/* jshint esversion : 6 */
var Pitch = require('./../model/pitch.model');
var Team = require('./../model/team.model');
var User = require('./../model/user.model');
var successMessage = require('../services/successMessage');
var failMessage = require('../services/failMessage');
var geolib = require('geolib');

module.exports = {
    createPitch: createPitch,
    getPitch: getPitch,
    updatePitch: updatePitch,
    deletePitch: deletePitch,
    addFeedback: addFeedback,
    updateFeedback: updateFeedback,
    removeFeedback: removeFeedback,
    getAllPitch: getAllPitch,
    getPitchDistance: getPitchDistance
};

function createPitch(request) {
    return Pitch.findOne({
        name: request.name
    }).exec().then(function (pitch) {
        if (pitch) {
            return Promise.reject({
                statusCode: 400,
                message: failMessage.pitch.create.duplicatePitch
            });
        }
        var newPitch = new Pitch({
            name: request.name,
            image: request.image,
            address: request.address,
            hotline: request.hotline,
            quanlity: request.quanlity,
            description: request.description
        });
        return newPitch.save()
            .then(() => ({
                messsage: successMessage.pitch.create
            }))
            .catch(() => ({
                messsage: failMessage.pitch.create.systemErr
            }));
    });
}

function getAllPitch() {
    return Pitch.find().exec();
}

function getPitch(request) {
    return Pitch.findOne({
        _id: request.pitchId
    }).exec();
}

function getPitchDistance(request){
    var list = [];
    return Team.findOne({
        _id: request.teamId
    }).exec().then(function (team) {
        return Pitch.find().exec().then(function(listPitch){
            listPitch.forEach(function(value){
                 var distance = geolib.getDistance(team.location, value.location);
                 var element = {
                        _id : value._id,
                        name: value.name,
                        address: value.address,
                        location: value.location,
                        hotline: value.hotline,
                        distance: distance
                    };
                list.push(element);
            })
            list.sort(function(a,b){
                return (a.distance - b.distance)
            })
            return Promise.resolve(list);
        })
})
}

function updatePitch(request) {

    return Pitch.findOne({
        _id: request.pitchId
    }).exec()
        .then((pitch) => {
            var checkDup = false;
            if (pitch.name === request.name) {
                checkDup = true;
            }
            pitch.name = request.name;
            pitch.image = request.image || pitch.image;
            pitch.address = request.address || pitch.address;
            pitch.hotline = request.hotline || pitch.hotline;
            pitch.quanlity = request.quanlity || pitch.quanlity;
            pitch.description = request.description || pitch.description;

            if (checkDup === true) {
                return pitch.save();
            }
            return Pitch.count({
                name: request.name
            }).exec().then((count) => {

                if (count === 1) {
                    return Promise.reject({
                        statusCode: 400,
                        msg: failMessage.pitch.create.duplicatePitch
                    });
                }
                return pitch.save();
            });
        });
}

function deletePitch(request) {
    return Pitch.findOneAndRemove({ _id: request.pitchId }).exec()
}

function addFeedback(request) {

    return Pitch.findOne({ _id: request.pitchId })
        .then((pitch) => {
            if (pitch === null) {
                console.log("pitch: " + pitch);
                return Promise.reject({
                    statusCode: 404,
                    message: failMessage.pitch.get.notFound
                });
            }
            return User.findOne({ _id: request.userId })
                .then((user) => {
                    if (user === null) {
                        return Promise.reject({
                            statusCode: 404,
                            message: failMessage.user.notFound
                        });
                    }

                    var newFeedback = {
                        userId: request.userId,
                        vote: request.vote,
                        comment: request.comment
                    };

                    return Pitch.findOne({ _id: request.pitchId, 'feedback.userId': request.userId })
                        .then((feedback) => {
                            if (feedback !== null) {
                                return Promise.reject({
                                    statusCode: 400,
                                    message: failMessage.pitch.create.duplicateFeedback
                                });
                            }

                            return Pitch.update({ _id: request.pitchId }, { $push: { feedback: newFeedback } })
                                .then(() => {
                                    console.log("Aaaaa");
                                    return Promise.resolve({
                                        message: successMessage.pitch.addFeedback
                                    });
                                })
                                .catch((err) => {
                                    return Promise.reject({
                                        message: failMessage.pitch.create.systemErr
                                    });
                                });
                        })
                        .catch((err) => {
                            return Promise.reject({
                                message: failMessage.pitch.create.systemErr
                            });
                        });
                })
                .catch((err) => ({
                    message: failMessage.pitch.create.systemErr
                }));
        })
        .catch((err) => {
            return Promise.reject({
                message: failMessage.pitch.create.systemErr
            });
        });
}

function updateFeedback(request) {
    return Pitch.findOne({ _id: request.pitchId })
        .then((pitch) => {
            if (pitch === null) {
                return Promise.reject({
                    message: failMessage.pitch.get.notFound
                });
            }

            return User.findOne({ _id: request.userId })
                .then((user) => {
                    if (user === null) {
                        return Promise.reject({
                            message: failMessage.user.login.notFound
                        });
                    }

                    return Pitch.findOne({
                        _id: request.pitchId,
                        'feedback.userId': request.userId
                    })
                        .then((feedback) => {
                            if (feedback === null) {
                                return Promise.reject({
                                    message: failMessage.pitch.addFeedback.notFound
                                });
                            }

                            var newFeedback = {
                                vote: request.vote,
                                comment: request.comment
                            };

                            feedback.feedback.forEach(function (value) {
                                if (value.userId.equals(request.userId)) {
                                    if (request.vote === undefined) {
                                        newFeedback.vote = value.vote;
                                    }
                                    if (request.comment === undefined) {
                                        newFeedback.comment = value.comment;
                                    }
                                }
                            });
                            return Pitch.update({
                                _id: request.pitchId,
                                'feedback.userId': request.userId
                            }, {
                                    $set: {
                                        'feedback.$.vote': newFeedback.vote,
                                        'feedback.$.comment': newFeedback.comment
                                    }
                                })
                                .then(() => {
                                    return Promise.resolve({
                                        message: successMessage.pitch.updateFeedback
                                    });
                                })
                                .catch((err) => {
                                    return Promise.reject({
                                        message: failMessage.pitch.create.systemErr
                                    });
                                })

                        })
                        .catch((err) => {
                            return Promise.reject({
                                message: failMessage.pitch.create.systemErr
                            });
                        })

                })
                .catch((err) => {
                    return Promise.reject({
                        message: failMessage.pitch.create.systemErr
                    });
                })

        })
        .catch((err) => {
            return Promise.reject({
                message: failMessage.pitch.create.systemErr
            });
        });
}

function removeFeedback(request) {
    return Pitch.findOne({ _id: request.pitchId })
        .then((pitch) => {
            if (pitch === null) {
                return Promise.reject({
                    statusCode: 404,
                    message: failMessage.pitch.get.notFound
                });
            }

            return User.findOne({ _id: request.userId })
                .then((user) => {
                    if (user === null) {
                        return Promise.reject({
                            statusCode: 404,
                            message: failMessage.user.login.notFound
                        });
                    }

                    return Pitch.update({
                        _id: request.pitchId
                    }, {
                            $pull: {
                                'feedback': {
                                    userId: request.userId
                                }
                            }
                        }
                    )
                        .then((res) => {
                            return Promise.resolve({
                                message: successMessage.pitch.removeFeedback
                            });
                        })
                        .catch((err) => {
                            return Promise.reject({
                                message: failMessage.pitch.create.systemErr
                            });
                        })

                })
                .catch((err) => {
                    return Promise.reject({
                        message: failMessage.pitch.create.systemErr
                    });
                });
        })
        .catch((err) => {
            return Promise.reject({
                message: failMessage.pitch.create.systemErr
            });
        })
}

// function deletePitch(request, callback) {
//     if (request.pitchId === '') {
//         callback({
//             statusCode: 403,
//             message: failMessage.pitch.create.input
//         });
//     } else {
//         Pitch.findOneAndRemove({
//             _id: request.pitchId
//         }, function(err) {
//             if (err) {
//                 callback({
//                     message: failMessage.pitch.create.systemErr
//                 });
//             } else {
//                 callback({
//                     message: successMessage.pitch.delete
//                 });
//             }
//         });
//     }
// }

// function addFeedback(request, callback) {
//     if (request.pitchId === '' || request.userId === '' || request.vote === '' || request.comment === '') {
//         callback({
//             statusCode: 403,
//             message: failMessage.pitch.create.input
//         });
//     } else {
//         Pitch.findOne({
//             _id: request.pitchId
//         }, function(err, pitch) {
//             if (err) {
//                 callback({
//                     message: failMessage.pitch.create.systemErr
//                 });
//             } else {
//                 if (pitch === null) {
//                     callback({
//                         statusCode: 404,
//                         message: failMessage.pitch.get.notFound
//                     });
//                 } else {
//                     User.findOne({
//                         _id: request.userId
//                     }, function(err, user) {
//                         if (err) {
//                             callback({
//                                 message: failMessage.pitch.create.systemErr
//                             });
//                         } else {
//                             if (user === null) {
//                                 callback({
//                                     statusCode: 404,
//                                     message: failMessage.user.notFound
//                                 });
//                             } else {

//                                 var newFeedback = {
//                                     userId: request.userId,
//                                     vote: request.vote,
//                                     comment: request.comment
//                                 };

//                                 Pitch.findOne({
//                                     _id: request.pitchId,
//                                     'feedback.userId': request.userId
//                                 }, function(err, feedback) {
//                                     if (err) {
//                                         callback({
//                                             message: failMessage.pitch.create.systemErr
//                                         });
//                                     } else {
//                                         if (feedback !== null) {
//                                             callback({
//                                                 statusCode: 400,
//                                                 message: failMessage.pitch.create.duplicateFeedback
//                                             });
//                                         } else {
//                                             Pitch.update({
//                                                 _id: request.pitchId
//                                             }, {
//                                                 $push: {
//                                                     feedback: newFeedback
//                                                 }
//                                             }, function(err) {
//                                                 if (err) {
//                                                     callback({
//                                                         message: failMessage.pitch.create.systemErr
//                                                     });
//                                                 } else {
//                                                     callback({
//                                                         message: successMessage.pitch.addFeedback
//                                                     });
//                                                 }
//                                             });
//                                         }
//                                     }
//                                 });
//                             }
//                         }
//                     });
//                 }
//             }
//         });
//     }
// }

// function updateFeedback(request, callback) {
//     if (request.pitchId === '' || request.userId === '' || request.vote === '' || request.comment === '') {
//         callback({
//             statusCode: 403,
//             message: failMessage.pitch.create.input
//         });
//     } else {
//         Pitch.findOne({
//             _id: request.pitchId
//         }, function(err, pitch) {
//             if (err) {
//                 callback({
//                     message: failMessage.pitch.create.systemErr
//                 });
//             } else {
//                 if (pitch === null) {
//                     callback({
//                         message: failMessage.pitch.get.notFound
//                     });
//                 } else {
//                     console.log(request.userId);
//                     User.findOne({
//                         _id: request.userId
//                     }, function(err, user) {
//                         if (err) {
//                             callback({
//                                 message: failMessage.pitch.create.systemErr
//                             });
//                         } else {
//                             if (user === null) {
//                                 callback({
//                                     message: failMessage.user.login.notFound
//                                 });
//                             } else {
//                                 Pitch.findOne({
//                                     _id: request.pitchId,
//                                     'feedback.userId': request.userId
//                                 }, function(err, feedback) {
//                                     if (err) {
//                                         callback({
//                                             message: failMessage.pitch.create.systemErr
//                                         });
//                                     } else {
//                                         if (feedback === null) {
//                                             callback({
//                                                 message: failMessage.pitch.addFeedback.notFound
//                                             });
//                                         } else {

//                                             var newFeedback = {
//                                                 vote: request.vote,
//                                                 comment: request.comment
//                                             };

//                                             feedback.feedback.forEach(function(value) {

//                                                 if (value.userId.equals(request.userId)) {
//                                                     if (request.vote === undefined) {
//                                                         newFeedback.vote = value.vote;
//                                                     }
//                                                     if (request.comment === undefined) {
//                                                         newFeedback.comment = value.comment;
//                                                     }
//                                                 }
//                                             });
//                                             console.log(newFeedback);
//                                             Pitch.update({
//                                                 _id: request.pitchId,
//                                                 'feedback.userId': request.userId
//                                             }, {
//                                                 $set: {
//                                                     'feedback.$.vote': newFeedback.vote,
//                                                     'feedback.$.comment': newFeedback.comment
//                                                 }
//                                             }, function(err) {
//                                                 if (err) {
//                                                     callback({
//                                                         message: failMessage.pitch.create.systemErr
//                                                     });
//                                                 } else {
//                                                     callback({
//                                                         message: successMessage.pitch.updateFeedback
//                                                     });
//                                                 }
//                                             });
//                                         }
//                                     }
//                                 });
//                             }
//                         }
//                     });
//                 }
//             }
//         });
//     }
// }

// function removeFeedback(request, callback) {
//     if (request.pitchId === '' || request.userId === '') {
//         callback({
//             statusCode: 403,
//             message: failMessage.pitch.create.input
//         });
//     } else {
//         Pitch.findOne({
//             _id: request.pitchId
//         }, function(err, pitch) {
//             if (err) {
//                 callback({
//                     message: failMessage.pitch.create.systemErr
//                 });
//             } else {
//                 if (pitch === null) {
//                     callback({
//                         statusCode: 404,
//                         message: failMessage.pitch.get.notFound
//                     });
//                 } else {
//                     console.log(request.userId);
//                     User.findOne({
//                         _id: request.userId
//                     }, function(err, user) {
//                         if (err) {
//                             callback({
//                                 message: failMessage.pitch.create.systemErr
//                             });
//                         } else {
//                             if (user === null) {
//                                 callback({
//                                     statusCode: 404,
//                                     message: failMessage.user.login.notFound
//                                 });
//                             } else {
//                                 Pitch.update({
//                                     _id: request.pitchId
//                                 }, {
//                                     $pull: {
//                                         'feedback': {
//                                             userId: request.userId
//                                         }
//                                     }
//                                 }, function(err, res) {
//                                     if (err) {
//                                         callback({
//                                             message: failMessage.pitch.create.systemErr
//                                         });
//                                     } else {
//                                         callback({
//                                             message: successMessage.pitch.removeFeedback
//                                         });
//                                     }
//                                 });
//                             }
//                         }
//                     });
//                 }
//             }
//         });
//     }
// }
