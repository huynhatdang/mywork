/*jshint esversion: 6*/
var Team = require('./../model/team.model');
var User = require('./../model/user.model');
var successMessage = require('../services/successMessage');
var failMessage = require('../services/failMessage');
var mongoose = require('mongoose');
var moment = require('moment');


module.exports = {
  createTeam: createTeam,
  getTeam: getTeam,
  updateTeam: updateTeam,
  deleteTeam: deleteTeam,
  addMemberTeam: addMemberTeam,
  removeMemberTeam: removeMemberTeam,
  getTeamRank: getTeamRank,
  getMyTeam: getMyTeam,
  getAnotherTeam: getAnotherTeam,
  joinTeam: joinTeam,
  getJoin: getJoin,
  acceptJoinTeam: acceptJoinTeam,
  getMessage: getMessage
};

function createTeam(request) {
  console.log(request);
  // return uploadLogo(request).then((urlLogo) => {

  return Team.findOne({
    name: request.name
  }).exec().then((team) => {
    if (team) {
      return Promise.reject({
        statusCode: 400,
        message: failMessage.team.create.duplicateTeam
      });
    }

    var newTeam = new Team({
      name: request.name,
      date: moment().format(),
      logo: request.fileId,
      // logo: request.logo,
      // colours: request.colours,
      // gallary: request.gallary,
      members: [request.owner]
    });
    newTeam.location.longitude = request.location[0];
    newTeam.location.latitude = request.location[1];

    return newTeam.save().then(() => {
      return Promise.resolve({
        messsage: successMessage.team.create
      });
    }).catch((err) => {
      console.log(err);
    });
    // });
  });
}

function uploadLogo(request) {
  return new Promise((resolve, reject) => {
    return resolve();
  });
}

function getTeam(request) {

  return Team.findOne({
    _id: request.teamId
  }).populate('members.userId').exec().then(function (team) {
    if (!team) {
      return Promise.reject({
        statusCode: 404,
        message: failMessage.team.create.teamNotFound
      });
    }

    return Promise.resolve({
      team: team,
      message: successMessage.team.getTeam
    });
  });
}

function updateTeam(request) {
  return Team.findOne({
    _id: request.teamId
  }).exec().then(function (team) {
    if (!team) {
      return Promise.reject({
        statusCode: 404,
        message: failMessage.team.create.teamNotFound
      });
    }
    var checkDup = false;
    if (team.name === request.name) {
      checkDup = true;
    }
    team.name = request.name;
    team.date = request.date || team.date;
    team.logo = request.logo || team.logo;
    team.colours = request.colours || team.colours;
    team.gallary = request.gallary || team.gallary;
    if (checkDup === true) {
      return team.save().then(function (msg) {
        return Promise.resolve({
          message: successMessage.team.update
        });
      });
    }
    return Team.count({
      name: request.name
    }).exec().then(function (count) {
      if (count === 1) {
        return Promise.reject({
          statusCode: 400,
          message: failMessage.team.update.duplicateTeam
        });
      }
      return team.save().then(function (msg) {
        return Promise.resolve({
          message: successMessage.team.update
        });
      });
    });
  });
}

function deleteTeam(request) {
  return Team.findOneAndRemove({
    _id: request.teamId
  }, function (err) {
    if (err) {
      return Promise.reject({
        statusCode: 404,
        message: failMessage.team.create.teamNotFound
      });
    }
    return Promise.resolve({
      message: successMessage.team.removeTeam
    });
  });
}

function addMemberTeam(request) {
  console.log(request);
  return Team.findOne({
    _id: request.teamId
  }).exec().then(function (team) {
    if (!team) {
      return Promise.reject({
        statusCode: 404,
        message: failMessage.team.create.teamNotFound
      });
    }
    var member = {
      userId: request.userId,
      role: request.role,
      position: request.position,
      playerNumber: request.playerNumber
    };
    return User.findById({
      _id: request.userId
    }).exec().then(function (user) {
      if (!user) {
        return Promise.reject({
          statusCode: 404,
          message: failMessage.team.create.userNotFound
        });
      }

      // check user duplicate
      return Team.findOne({
        _id: request.teamId,
        'members.userId': request.userId
      }).exec().then(function (mem) {
        if (mem !== null) {
          return Promise.reject({
            statusCode: 400,
            message: failMessage.team.create.duplicateMember
          });
        }
        return Team.update({
          _id: request.teamId
        }, {
            $push: {
              members: member
            }
          }).exec().then(function (err) {
            return Promise.resolve({
              message: successMessage.team.addMember
            });
          });
      });
    });
  });
}

function removeMemberTeam(request) {

  return Team.findOne({
    _id: request.teamId
  }).exec().then(function (team) {
    if (!team) {
      return Promise.reject({
        statusCode: 404,
        message: failMessage.team.remove.notFound
      });
    }
    // check role before remove
    Team.update({
      _id: request.teamId
    }, {
        $pull: {
          'members': {
            userId: request.userId
          }
        }
      }).exec().then(function (err) {
        return Promise.resolve({
          message: successMessage.team.removeMember
        });
      });
  });
}

function getTeamRank(request) {
  return Team.find({}).sort({ score: -1 }).limit(parseInt(request.limit)).exec().then((team) => ({
    team: team,
    message: successMessage.team.getTeam
  })).catch(() => ({
    statusCode: 404,
    message: failMessage.team.create.teamNotFound
  }))
}

function getMyTeam(request) {
  return Team.find({ 'members.userId': request.id }).populate('requestJoinTeam.userId').exec().then(function (team) {
    var list = [];
    team.forEach(function (value) {
      var element = {
        _id: value._id,
        name: value.name,
        members: value.members,
        logo: value.logo,
        score: value.score,
        isOwner: false,
        requestJoinTeam: value.requestJoinTeam,
        message: value.message
      };
      for (var checkOwn in value.members) {
        if (value.members[checkOwn].userId == request.id && value.members[checkOwn].role == 'OWNER') {
          element.isOwner = true;
        }
      }
      list.push(element);
    }, this);
    return Promise.resolve({
      team: list,
      message: successMessage.team.getTeam
    })
  }).catch(() => ({
    statusCode: 404,
    message: failMessage.team.create.teamNotFound
  }))
}

function getMessage(request) {
  var listMessage=[];
  return Team.find({ 'members.userId': request.id }, { message: 1, _id: 0 }).exec().then(function (list) {
    // listMessage= list;
    list.forEach(function(e){
      if(e.message.length>0){
        e.message.forEach(function(f){
          listMessage.push(f)
        })
      }
    })
    listMessage.sort(function(a,b){
      return (b.createdTime - a.createdTime)
    })
    return Promise.resolve({
      listMessage:listMessage
    })
  })
}

function getAnotherTeam(request) {
  return Team.find({ 'members.userId': { '$ne': request.id } }).exec().then((team) => ({
    team: team,
    message: successMessage.team.getTeam
  })).catch(() => ({
    statusCode: 404,
    message: failMessage.team.create.teamNotFound
  }))
}

function joinTeam(request) {
  var check = false;
  return Team.findById(request.teamId).exec().then(function (team) {
    if (team) {
      team.requestJoinTeam.forEach(function (e) {
        if (e.userId == request.userId) {
          return check = true;
        }
      })
      if (check == true) {
        return Promise.reject({
          statusCode: 400,
          message: failMessage.team.create.joinExist
        })
      }
      var obj = {
        userId: request.userId
      };
      team.requestJoinTeam.push(obj);
      return team.save().then(() => ({
        statusCode: 200,
        message: successMessage.team.joinTeam
      })).catch(() => ({
        statusCode: 404,
        message: failMessage.team.create.notFound
      }));
    }
  }).catch((err) => ({
    statusCode: 404,
    message: failMessage.team.create.notFound
  }))
}

function getJoin(request) {
  return Team.findById(request.teamId).populate('requestJoinTeam.userId').exec().then((team) => ({
    statusCode: 200,
    team: team
  })).catch(err => ({
    statusCode: 404,
    message: failMessage.team.create.notFound
  }))
}

function acceptJoinTeam(request) {
  return addMemberTeam(request).then(function () {
    console.log(111);
    return Team.findByIdAndUpdate(request.teamId, {
      $pull: {
        requestJoinTeam: { _id: request.requestId }
      }
    }).then(() => ({
      statusCode: 200,
      message: successMessage.team.addMember
    }))
  })
}
