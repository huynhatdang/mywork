/* jshint esversion : 6 */
var Work = require('./../model/work.model');
var User = require('./../model/user.model');
var Company = require('./../model/company.model');
var successMessage = require('../services/successMessage');
var failMessage = require('../services/failMessage');
var geolib = require('geolib');
var exchange = require("exchange-rates"),
    fx = require("money");

module.exports = {
    createWork: createWork,
    getWork: getWork,
    updateWork: updateWork,
    deleteWork: deleteWork,
    searchWork: searchWork,
    addFavorite: addFavorite,
    getFavorite: getFavorite,
    sendCV: sendCV
};

function createWork(request) {
    if (request.salary.type === "USD") {
        var from = request.salary.salaryFrom * 22700;
        var to = request.salary.salaryTo * 22700;
        request.salary.averageSalary = (from + to) / 2;
    }
    else {
        request.salary.averageSalary = (request.salary.salaryFrom + request.salary.salaryTo) / 2;
    }
    var company;
    return User.findById(request.user._id).exec().then((user) => {
        var newWork = new Work({
            name: request.name,
            dateTime: request.dateTime,
            position: request.position,
            salary: request.salary,
            requirement: request.requirement,
            benifit: request.benifit,
            major: request.major,
            description: request.description,
            company: user.company
        });

        console.log(newWork);
        return saveWork(newWork, user.company)
    }).catch(() => ({
        msg: failMessage.work.create.systemErr
    }));
};

function saveWork(newWork, companyId) {
    return newWork.save()
        .then((work) => {
            return Company.findOneAndUpdate({ _id: companyId }, { $push: { "work": work._id } }).exec().then(() => ({
                msg: successMessage.work.create
            })).catch(() => ({
                msg: failMessage.work.create.systemErr
            }))
        })
}


function getWork(request) {
    return Work.findOne({
        _id: request.workId
    }).populate('company').exec();
}

function getFavorite(request) {
    console.log(request.userId);
    return User.findById(request.userId).select('favorite').populate({
        path: 'favorite',
        populate: {
            path: 'company',
            model: 'company'
        }
    }).exec().then((favorite) => ({
        favorite: favorite,
        msg: successMessage.work.get
    })).catch(() => ({
        statusCode: 404,
        msg: failMessage.work.get.notFound
    }))
}

function addFavorite(request) {
    return User.findById(request.userId).exec().then((user) => {
        var index = user.favorite.indexOf(request.workId)
        if (index !== -1) {
            console.log(index);
            user.favorite.splice(index, 1);
            user.save();
        }
        else {
            user.favorite.push(request.workId);
            user.save();
        }
    })
}

function sendCV(request){
    return User.findById(request.userId).exec().then((user)=>{
        if(user.cv){
            return Work.findById(request.workId).exec().then((work)=>{
                if (work.cvSent.indexOf(user.cv) === -1 ){
                    work.cvSent.push(user.cv);
                    work.save()
                }
            }).catch((err)=>({
                msg: failMessage.work.get.notFound,
                statusCode: 404
            }))
        } else {
            return ({
                msg: failMessage.cv.get.notFound,
                statusCode: 404
            })
        }
    })

}

function searchWork(request) {
    return User.findById(request.user._id).then((user) => {
        return Work.find({
            $text: { $search: request.search }
        }, {
                score: { $meta: "textScore" }
            }).sort({ score: { $meta: "textScore" } }).populate('company').exec().then((works) => {
                if (works.length === 0) {
                    return Work.find({ 'name': new RegExp(request.search, 'i') })
                        .populate('company').exec().then(work => {
                            return sortDis(work, user.address.location, user.favorite).then((workDistance) => {
                                if (request.searchBy === "salary") {
                                    return workDistance.sort((a, b) => {
                                        return b.salary.averageSalary - a.salary.averageSalary
                                    })
                                    // return work.sort({'salary.averageSalary': -1}).exec().then((workSalary) => console.log(workSalary)).catch((err) => err)
                                } else if (request.searchBy === "address") {
                                    return workDistance.sort((a, b) => {
                                        return (a.distance - b.distance);
                                    })
                                } else {
                                    return workDistance
                                }
                            })
                        })
                } else {
                    return sortDis(works, user.address.location, user.favorite).then((workDistance) => {
                        if (request.searchBy === "salary") {
                            return workDistance.sort((a, b) => {
                                return b.salary.averageSalary - a.salary.averageSalary
                            })
                            // return works.sort({ 'salary.averageSalary': -1 }).then((workSalary) => workSalary).catch((err) => err)
                        } else if (request.searchBy === "address") {
                            return workDistance.sort((a, b) => {
                                return (a.distance - b.distance);
                            })
                        } else {
                            return workDistance
                        }
                    })

                }
            }).catch(() => ({
                statusCode: 404,
                msg: failMessage.work.get.notFound
            }))
    })
}

function sortDis(listWork, userLocation, favorite) {
    console.log(listWork.length);
    var list = [];
    for (var i = 0; i < listWork.length; i++) {
        var value = listWork[i];
        if (value.company) {
            var distance = geolib.getDistance(userLocation, value.company.address.location);
            var checkFavorite = false;
            if (favorite.indexOf(value._id) !== -1) {
                checkFavorite = true;
            };
            var element = {
                _id: value._id,
                name: value.name,
                dateTime: value.dateTime,
                position: value.position,
                salary: value.salary,
                major: value.major,
                distance: distance,
                company: value.company,
                benifit: value.benifit,
                description: value.description,
                requirement: value.requirement,
                favorite: checkFavorite
            };
            console.log(distance);
            list.push(element);
        }
    }
    // list.sort(function (a, b) {
    //     return (a.distance - b.distance);
    // });
    return Promise.resolve(list);

    // console.log(list);
}


function updateWork(request) {

    return Work.findOne({
        _id: request.workId
    }).exec()
        .then((work) => {
                work.name = request.name;
                work.dateTime = request.dateTime;
                work.position = request.position;
                work.salary = request.salary;
                work.requirement = request.requirement;
                work.benifit = request.benifit;
                work.major = request.major;
                work.description = request.description;

            return work.save();
        })
}

function deleteWork(request) {
    return Work.findOneAndRemove({ _id: request.workId }).exec()
}

