/* jshint esversion : 6 */
var Company = require('./../model/company.model');
var User = require('./../model/user.model');
var successMessage = require('../services/successMessage');
var failMessage = require('../services/failMessage');
var fs= require('fs');


module.exports = {
    createCompany: createCompany,
    getCompany: getCompany,
    updateCompany: updateCompany,
    deleteCompany: deleteCompany,
    uploadImage: uploadImage
};

function createCompany(request) {
    return User.findById(request.user._id).exec().then((user) => {
        console.log(user.company)
        if (!user.company) {
            var newCompany = new Company({
                name: request.name,
                major: request.major,
                contact: request.contact,
                address: request.address,
                dateTime: request.dateTime,
                human: request.human,
                scale: request.scale,
                description: request.description,
                website: request.website,
                image: "./public/huy.jpg"
            });

            return newCompany.save()
                .then((company) => {
                    return User.findOneAndUpdate({ _id: request.user._id }, { $set: { company: company._id } }).exec().then(() => ({
                        msg: successMessage.company.create
                    })).catch(() => {
                        return Promise.reject({
                            statusCode: 400,
                            msg: failMessage.company.create.systemErr
                        })
                    })
                        .catch(() => ({
                            statusCode: 400,
                            msg: failMessage.company.create.systemErr
                        }));
                })
        } else {
            return Promise.reject({
                statusCode: 404,
                msg: failMessage.company.create.existed
            })
        }

    })
}

function uploadImage(request){
    return User.findOneAndUpdate({ _id: request.user._id},{ $set:{ image: request.image}}).exec();
}


function getCompany(request) {
    return Company.findOne({
        _id: request.companyId
    }).exec();
}


function updateCompany(request) {

    return Company.findOne({
        _id: request.companyId
    }).exec()
        .then((company) => {
            var checkDup = false;
            if (company.name === request.name) {
                checkDup = true;
            }
            company.name = request.name;
            company.contact = request.contact || company.contact;
            company.address = request.address || company.address;
            company.dateTime = request.dateTime || company.dateTime;
            company.human = request.human || company.human;
            company.description = request.description || company.description;
            company.scale = request.scale || company.scale;
            company.website = request.website || company.website;

            if (checkDup === true) {
                return company.save();
            }
            return Company.count({
                name: request.name
            }).exec().then((count) => {

                if (count === 1) {
                    return Promise.reject({
                        statusCode: 400,
                        msg: failMessage.company.create.duplicateCompany
                    });
                }
                return company.save();
            });
        });
}

function deleteCompany(request) {
    return Company.findOneAndRemove({ _id: request.companyId }).exec()
}

