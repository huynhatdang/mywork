/* jshint esversion : 6 */
var CV = require('./../model/cv.model');
var User = require('./../model/user.model');
var successMessage = require('../services/successMessage');
var failMessage = require('../services/failMessage');

module.exports = {
    createCV: createCV,
    getCV: getCV,
    updateCV: updateCV,
    deleteCV: deleteCV,
    checkCV: checkCV
};

function createCV(request) {
    var newCV = new CV({
        certificate: request.certificate,
        experience: request.experience,
        language: request.language,
        user: request.userId
    });
    return newCV.save()
        .then((cv) => {
            return User.findByIdAndUpdate(request.userId,{$set: { cv : cv._id}}).exec().then(() => ({
                msg: successMessage.cv.create
            })).catch((err)=>({
                msg: failMessage.user.login.notFound
            }))
        }
        )
        .catch(() => ({
            msg: failMessage.cv.create.systemErr
        }));
}


function getCV(request) {
    return CV.findOne({
        _id: request.cvId
    }).populate('user').exec().then((cv)=>({
        msg: successMessage.cv.get,
        cv: cv
    })).catch(()=>({
        statusCode: 404,
        msg: failMessage.cv.create.notFound
    }));
}

function checkCV(request) {
    console.log(request);
    return User.findById(request.userId).populate('cv').exec().then((user) => {
        console.log(user);
        if (user.cv) {
            return Promise.resolve({
                cv: user.cv
            })
        } else {
            return Promise.reject({
                statusCode: 404,
                msg: 'Chưa tạo CV'
            })
        }
    })
}


function updateCV(request) {

    return CV.findOne({
        _id: request.cvId
    }).exec()
        .then((cv) => {
            cv.certificate = request.certificate;
            cv.experience = request.experience;
            cv.language = request.language;

            return cv.save();
        });
}

function deleteCV(request) {
    return CV.findOneAndRemove({ _id: request.cvId }).exec()
}

