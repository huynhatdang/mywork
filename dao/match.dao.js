/*jshint esversion: 6*/
var Match = require('./../model/match.model');
var Team = require('./../model/team.model');
var Pitch = require('./../model/pitch.model');
var successMessage = require('../services/successMessage');
var failMessage = require('../services/failMessage');
var moment = require('moment');
var geolib = require('geolib');
var pagination = require('../services/pagination');

module.exports = {
    createMatch: createMatch,
    createChallenged: createChallenged,
    getMatch: getMatch,
    updateMatch: updateMatch,
    deleteMatch: deleteMatch,
    getMatchOngoing: getMatchOngoing,
    getPagination: getPagination,
    acceptChallenger: acceptChallenger,
    getChallengerMatch: getChallengerMatch,
    getAcceptedMatch: getAcceptedMatch
};

function createMatch(request) {
    if (request.hometeamId === request.awayteamId) {
        return Promise.reject({
            statusCode: 400,
            message: failMessage.match.create.duplicateTeam
        });
    }

    return Team.findOne({
        _id: request.hometeamId
    }).exec().then(function (hometeam) {
        if (!hometeam) {
            return Promise.reject({
                statusCode: 404,
                message: failMessage.match.create.notFound
            });
        }
        return Team.findOne({
            _id: request.awayteamId
        }).exec().then(function (awayteam) {
            if (!awayteam) {
                return Promise.reject({
                    statusCode: 404,
                    message: failMessage.match.create.notFound
                });
            }
            return Pitch.findOne({
                _id: request.pitchId
            }).exec().then(function (pitch) {
                if (!pitch) {
                    return Promise.reject({
                        statusCode: 404,
                        message: failMessage.match.create.notFound
                    });
                }
                return Match.findOne({
                    datetime: request.datetime,
                    $or: [{
                        hometeamId: request.hometeamId
                    },
                    {
                        awayteamId: request.awayteamId
                    }
                    ]
                }).exec().then(function (match) {
                    if (match) {
                        return Promise.reject({
                            statusCode: 400,
                            message: failMessage.match.create.matchExist
                        });
                    }
                    var newMatch = new Match({
                        hometeamId: request.hometeamId,
                        awayteamId: request.awayteamId,
                        pitchId: request.pitchId,
                        datetime: request.datetime,
                        type: request.type,
                        status: request.status,
                        result: request.result
                    });

                    return newMatch.save().then(function () {
                        return Promise.resolve({
                            message: successMessage.match.create
                        });
                    });
                });
            });
        });
    });
}

function createChallenged(request) {
    var today = moment().format();
    if (moment(request.datetime).isBefore(today)) {
        return Promise.reject({
            statusCode: 400,
            message: failMessage.match.create.timeIsInvalid
        });
    }
    var newMatch = new Match({
        hometeamId: request.hometeamId,
        awayteamId: request.awayteamId || null,
        pitchId: request.pitchId || null,
        datetime: request.datetime,
        type: request.type,
        status: request.status,
        result: request.result || ''
    });
    return Match.find({
        // 'datetime': {
        //     '$lte': new Date(request.datetime)
        // },
        $or: [
            { 'hometeamId': request.hometeamId },
            { 'awayteamId': request.hometeamId }
        ],
        'status': 'accepted'
    }).sort({ 'datetime': -1 }).exec().then(function (match) {
        if (match.length > 0) {
            var count = 0;
            match.forEach(function (e) {
                if (moment(e.datetime).isBetween(moment(request.datetime).subtract(2, 'hours'), moment(request.datetime).add(2, 'hours'))) {
                    count++;
                }
            })
            if (count > 0) {
                return Promise.reject({
                    statusCode: 400,
                    message: failMessage.match.create.matchExist
                });
            }
            else {
                if (request.awayteamId) {
                    return Match.find({
                        // 'datetime': {
                        //     '$lte': new Date(request.datetime)
                        // },
                        $or: [
                            { 'hometeamId': request.awayteamId },
                            { 'awayteamId': request.awayteamId }
                        ],
                        'status': 'accepted'
                    }).sort({ 'datetime': -1 }).exec().then(function (match) {
                        if (match.length > 0) {
                            var count = 0;
                            match.forEach(function (e) {
                                if (moment(e.datetime).isBetween(moment(request.datetime).subtract(2, 'hours'), moment(request.datetime).add(2, 'hours'))) {
                                    count++;
                                }
                            });
                            console.log(count);
                            if (count > 0) {
                                return Promise.reject({
                                    statusCode: 400,
                                    message: failMessage.match.create.matchExist
                                });
                            }

                            else {
                                return newMatch.save().then(function () {
                                    return Promise.resolve({
                                        message: successMessage.match.create
                                    });
                                });
                            }
                        }
                        // console.log(newMatch);
                        return newMatch.save().then(function () {
                            return Promise.resolve({
                                message: successMessage.match.create
                            });
                        });
                    })
                } else {
                    return newMatch.save().then(function () {
                        return Promise.resolve({
                            message: successMessage.match.create
                        });
                    });
                }
            }
        } else {
            if (request.awayteamId) {
                return Match.find({
                    // 'datetime': {
                    //     '$lte': new Date(request.datetime)
                    // },
                    $or: [
                        { 'hometeamId': request.awayteamId },
                        { 'awayteamId': request.awayteamId }
                    ],
                    'status': 'accepted'
                }).sort({ 'datetime': -1 }).exec().then(function (match) {
                    if (match.length) {
                        var count = 0;
                        match.forEach(function (e) {
                            if (moment(e.datetime).isBetween(moment(request.datetime).subtract(2, 'hours'), moment(request.datetime).add(2, 'hours'))) {
                                count++;
                            }
                        })
                        if (count > 0) {
                            return Promise.reject({
                                statusCode: 400,
                                message: failMessage.match.create.matchExist
                            });
                        }
                        else {
                            return newMatch.save().then(function () {
                                return Promise.resolve({
                                    message: successMessage.match.create
                                });
                            });
                        }
                    }
                    // console.log(newMatch);
                    return newMatch.save().then(function () {
                        return Promise.resolve({
                            message: successMessage.match.create
                        });
                    });
                })
            } else {
                return newMatch.save().then(function () {
                    return Promise.resolve({
                        message: successMessage.match.create
                    });
                });
            }
        }
    })
}






// return Team.findOne({
//     _id: request.hometeamId
// }).exec().then(function (hometeam) {
//     if (!hometeam) {
//         return Promise.reject({
//             statusCode: 404,
//             message: failMessage.match.create.notFound
//         });
//     }
//     console.log(request.datetime)



function getMatch(request) {
    return Match.findOne({
        _id: request.matchId
    }).exec().then(function (match) {
        if (!match) {
            return Promise.reject({
                statusCode: 404,
                message: failMessage.match.create.notFound
            });
        }
        return Promise.resolve(function (msg) {
            return Promise.resolve({
                message: successMessage.match.get
            });
        });
    });
}

function checkAndDelete(datetime, teamId, homeaway) {
    return Match.find({
        // 'datetime': {
        //     '$lte': new Date(datetime)
        // },
        $or: [
            { 'hometeamId': teamId },
            { 'awayteamId': teamId }
        ],
        'status': 'accepted'
    }).sort({ 'datetime': -1 }).exec().then(function (match) {
        if (match.length > 0) {
            match.forEach(function (e) {
                if (moment(e.datetime).isBetween(moment(datetime).subtract(2, 'hours'), moment(datetime).add(2, 'hours'))) {
                    // console.log(111);
                    // deleteMatchAtTheSameTime(datetime, teamId);
                    if (homeaway == 1) {
                        return Promise.reject({
                            statusCode: 400,
                            message: failMessage.match.create.homeTeamBusy
                        });
                    }
                    return Promise.reject({
                        statusCode: 400,
                        message: failMessage.match.create.awayTeamBusy
                    });
                }
            })
        }
        return Promise.resolve({
            check: true
        });
    })
}

function deleteMatchAtTheSameTime(datetime, teamId) {
    return Match.find({
        $or: [
            { 'hometeamId': teamId },
            { 'awayteamId': teamId }
        ],
        'status': { $in: ['waiting', 'hasChallenger'] }
    }).sort({ 'datetime': -1 }).exec().then(function (match) {
        if (match.length > 0) {
            match.forEach(function (e) {
                if (moment(e.datetime).isBetween(moment(datetime).subtract(2, 'hours'), moment(datetime).add(2, 'hours'))) {
                    // console.log(e)
                    e.remove();
                }
            })

        }
    })
}

function updateAfter(match, request) {
    match.awayteamId = request.awayteamId;
    match.status = 'accepted';
    return match.save().then(() => ({
        statusCode: 200,
        message: successMessage.match.accept
    }))
}

function acceptChallenger(request) {
    return Match.findOne({
        _id: request.matchId
    }).populate('hometeamId').exec().then(function (match) {
        if (!match) {
            return Promise.reject({
                statusCode: 404,
                message: failMessage.match.create.notFound
            })
        } else {
            if (request.awayteamId && match.status == 'waiting') {
                // return checkAndDelete(match.datetime,request.awayteamId).then(checkAndDelete(match.datetime,match.hometeamId)).then(updateAfter(match,request)).catch(e=>console.log(e));

                return checkAndDelete(match.datetime, request.awayteamId, 2).then(function (checkAway) {
                    return checkAndDelete(match.datetime, match.hometeamId, 1).then(function (checkHome) {
                        console.log('success');
                        match.awayteamId = request.awayteamId;
                        match.status = 'accepted';
                        return match.save().then(function () {
                            Team.findById(request.awayteamId, function (err, t) {
                                Team.findByIdAndUpdate(match.hometeamId, {
                                    '$push': {
                                        "message": {
                                            message: t.name + ' has accepted your match at ' + moment(match.datetime).format('DD-MM-YYYY HH:mm') + ' with Team: ' + match.hometeamId.name,
                                            createdTime: moment().format()
                                        }
                                    }
                                }, function (err, suc) {
                                    if (err) {
                                        console.log(err)
                                    }
                                    console.log(suc);
                                })
                            })
                            deleteMatchAtTheSameTime(match.datetime, request.awayteamId).then(function () {
                                deleteMatchAtTheSameTime(match.datetime, match.hometeamId);
                            })
                            return Promise.resolve({
                                statusCode: 200,
                                message: successMessage.match.accept
                            });
                        }).catch((err) => ({
                            statusCode: 500,
                            message: failMessage.match.create.systemErr
                        }))
                    })
                })
            } else if (match.status == 'hasChallenger') {
                return checkAndDelete(match.datetime, match.awayteamId, 2).then(function (checkAway) {
                    return checkAndDelete(match.datetime, match.hometeamId, 1).then(function (checkHome) {
                        console.log('success');
                        match.status = 'accepted';
                        return match.save().then(function () {
                            Team.findById(match.awayteamId, function (err, t) {
                                Team.findByIdAndUpdate(match.hometeamId, {
                                    '$push': {
                                        "message": {
                                            message: t.name + ' has accepted your challenger at ' + moment(match.datetime).format('DD-MM-YYYY HH:mm') + ' with Team: ' + match.hometeamId.name,
                                            createdTime: moment().format()
                                        }
                                    }
                                }, function (err, suc) {
                                    if (err) {
                                        console.log(err)
                                    }
                                    console.log(suc);
                                })
                            })
                            deleteMatchAtTheSameTime(match.datetime, match.awayteamId).then(function () {
                                deleteMatchAtTheSameTime(match.datetime, match.hometeamId);
                            })
                            return Promise.resolve({
                                statusCode: 200,
                                message: successMessage.match.accept
                            });
                        }).catch((err) => ({
                            statusCode: 500,
                            message: failMessage.match.create.systemErr
                        }))
                    })
                })
            }
        }
    })
}

// function acceptChallenger(request) {
//     return Match.findOne({
//         _id: request.matchId
//     }).exec().then(function (match) {
//         if (!match) {
//             return Promise.reject({
//                 statusCode: 404,
//                 message: failMessage.match.create.notFound
//             })
//         }
//         if (request.awayteamId && match.status == 'waiting') {
//             match.awayteamId = request.awayteamId;
//             match.status = 'accepted';
//         } else if (match.status == 'hasChallenger') {
//             match.status = 'accepted';
//         }
//         return match.save().then(function () {
//             return Promise.resolve({
//                 message: successMessage.match.accept
//             });
//         });

//     }).catch((err) => ({
//         status: 404,
//         message: failMessage.match.create.systemErr
//     }))
// }

function updateMatch(request) {
    return Match.findOne({
        _id: request.matchId
    })
        .exec().then(function (match) {
            if (!match) {
                return Promise.reject({
                    statusCode: 404,
                    message: failMessage.match.create.notFound
                });
            }
            match.hometeamId = request.hometeamId || match.hometeamId;
            match.awayteamId = request.awayteamId || match.awayteamId;
            match.pitchId = request.pitchId || match.pitchId;
            match.datetime = request.datetime || match.datetime;
            match.type = request.type || match.type;
            match.status = request.status || match.status;
            match.result = request.result || match.result;

            if (match.hometeamId.equals(match.awayteamId)) {
                return Promise.reject({
                    statusCode: 400,
                    message: failMessage.match.create.duplicateTeam
                });
            }

            return Team.findOne({
                _id: request.hometeamId
            }).exec().then(function (hometeam) {
                if (!hometeam) {
                    return Promise.reject({
                        statusCode: 404,
                        message: failMessage.match.create.notFound
                    });
                }
                return Team.findOne({
                    _id: request.awayteamId
                }).exec().then(function (awayteam) {
                    if (!awayteam) {
                        return Promise.reject({
                            statusCode: 404,
                            message: failMessage.match.create.notFound
                        });
                    }

                    return Pitch.findOne({
                        _id: request.pitchId
                    }).exec().then(function (pitch) {
                        if (!pitch) {
                            return Promise.reject({
                                statusCode: 404,
                                message: failMessage.match.create.notFound
                            });
                        }
                        return Match.findOne({
                            datetime: match.datetime,
                            _id: {
                                $ne: request.matchId
                            },
                            $or: [{
                                hometeamId: request.hometeamId
                            },
                            {
                                awayteamId: request.awayteamId
                            }
                            ]
                        }).exec().then(function (match2) {
                            if (match2 !== null) {
                                return Promise.reject({
                                    statusCode: 400,
                                    message: failMessage.match.create.matchExist
                                });
                            }
                            return match.save().then(function () {
                                return Promise.resolve({
                                    message: successMessage.match.update
                                });
                            });
                        });
                    });
                });
            });
        });
}

function deleteMatch(request) {
    return Match.findOne({
        _id: request.matchId
    }, function (err, match) {
        if (match === null) {
            return Promise.reject({
                statusCode: 404,
                message: failMessage.match.get.notFound
            });
        }
        return Match.remove({
            _id: request.matchId
        }).exec().then(function (err) {
            return Promise.resolve({
                message: successMessage.match.remove
            });
        });
    });
}

function getMatchOngoing(request) {
    var today = moment().format();
    var list = [];

    return Team.findOne({
        _id: request.teamId
    }).exec().then(function (team) {
        return Match.find({
            datetime: {
                $gt: today
            },
            status: 'waiting'
        })
            .populate('hometeamId')
            .exec()
            .then(function (listTeam) {
                listTeam.forEach(function (value) {
                    var distance = geolib.getDistance(team.location, value.hometeamId.location);
                    var element = {
                        name: value.hometeamId.name,
                        logo: value.hometeamId.logo,
                        location: distance,
                        time: value.datetime,
                        type: value.type
                    };
                    if (value.hometeamId.name !== team.name) {
                        list.push(element);
                    }
                });
                list.sort(function (a, b) {
                    return (a.location - b.location);
                });
                return Promise.resolve(list);
            });
    });
}



// function getPagination(request) {
//     console.log(request);
//     var today = moment().utc().format();
//     var list = [];
//     var query = {
//         datetime: {
//             $gte: today
//         },
//         status: 'waiting'
//     };

//     return Match.count(query).then(function (count) {
//         return Match.find(query)
//             .populate('hometeamId')
//             .populate('pitchId')
//             .skip((request.pageIndex > 0) ? (request.pageIndex - 1) * request.pageSize : 0)
//             .limit(request.pageSize)
//             .exec().then(function (listTeam) {
//                 for (let value of listTeam) {
//                     var element = {
//                         matchId: value._id,
//                         name: value.hometeamId.name,
//                         time: value.datetime,
//                         type: value.type,
//                         pitch: value.pitchId.name
//                     };
//                     // console.log(element);
//                     var checkUserTeam = false;
//                     for (let valueUser of value.hometeamId.members) {
//                         if (valueUser.equals(request.userId)) {
//                             console.log(2);
//                             checkUserTeam = true;
//                             break;
//                         }
//                     }
//                     if (checkUserTeam === false) {
//                         console.log(1);
//                         list.push(element);
//                     }
//                 }
//                 console.log(list);
//                 list.sort(function (a, b) {
//                     return (a.time - b.time);
//                 });
//                 var res = pagination.pagination(list, count, request.pageIndex, request.pageSize);
//                 return Promise.resolve(res);
//             }).catch(function (err) {
//                 return Promise.reject({
//                     statusCode: 500,
//                     message: failMessage.match.create.systemErr
//                 });
//             });
//     }).catch(function (err) {
//         return Promise.reject({
//             statusCode: 500,
//             message: failMessage.match.create.systemErr
//         });
//     });
// }

function getPagination(request) {

    var today = moment().format();
    var list = [];
    var query = {
        datetime: {
            $gte: today
        },
        status: 'waiting'
    };

    return Match.count(query).then(function (count) {
        return Match.find(query)
            .populate('hometeamId')
            .populate('pitchId')
            // .skip((request.pageIndex > 0) ? (request.pageIndex - 1) * request.pageSize : 0)
            // .limit(request.pageSize)
            .exec().then(function (listTeam) {
                for (let value of listTeam) {
                    var element = {
                        matchId: value._id,
                        name: value.hometeamId.name,
                        time: value.datetime,
                        type: value.type,
                        pitch: value.pitchId ? value.pitchId.name : '',
                        logo: value.hometeamId.logo
                    };
                    var checkUserTeam = false;
                    for (let valueUser of value.hometeamId.members) {
                        if (valueUser.userId.equals(request.userId)) {
                            checkUserTeam = true;
                            break;
                        }
                    }
                    if (checkUserTeam == false) {
                        list.push(element);
                    }
                    // if(list.length == request.pageSize) break; 
                }

                list.sort(function (a, b) {
                    return (a.time - b.time);
                });
                var res = pagination.pagination(list, count, request.pageIndex, request.pageSize);
                return Promise.resolve(res);
            }).catch(function (err) {
                return Promise.reject({
                    statusCode: 500,
                    message: failMessage.match.create.systemErr
                });
            });
    }).catch(function (err) {
        return Promise.reject({
            statusCode: 500,
            message: failMessage.match.create.systemErr
        });
    });
}

function getChallengerMatch(request) {

    var today = moment().format();
    var list = [];
    var query = {
        datetime: {
            $gte: today
        },
        status: 'hasChallenger'
    };

    return Match.find(query)
        .populate('awayteamId')
        .populate('hometeamId')
        .populate('pitchId')
        // .skip((request.pageIndex > 0) ? (request.pageIndex - 1) * request.pageSize : 0)
        // .limit(request.pageSize)
        .exec().then(function (listTeam) {
            for (let value of listTeam) {
                var element = {
                    matchId: value._id,
                    homeTeam: value.hometeamId.name,
                    homeLogo: value.hometeamId.logo,
                    awayLogo: value.awayteamId.logo,
                    awayTeam: value.awayteamId.name,
                    time: value.datetime,
                    type: value.type,
                    pitch: value.pitchId ? value.pitchId.name : '',
                    status: value.status
                };
                var checkUserTeam = false;
                for (let valueUser of value.awayteamId.members) {
                    if (valueUser.userId.equals(request.userId)) {
                        checkUserTeam = true;
                        if (valueUser.role == 'OWNER') {
                            element.owner = true;
                        }
                        break;
                    }
                }
                if (checkUserTeam == true) {
                    list.push(element);
                }
                // if(list.length == request.pageSize) break; 
            }

            list.sort(function (a, b) {
                return (a.time - b.time);
            });
            return Promise.resolve(list);
        }).catch(function (err) {
            return Promise.reject({
                statusCode: 500,
                message: failMessage.match.create.systemErr
            });
        });

}

function getAcceptedMatch(request) {

    var today = moment().format();
    var list = [];
    var query = {
        datetime: {
            $gte: today
        },
        status: 'accepted'
    };

    return Match.find(query).sort({ datetime: 1 })
        .populate('awayteamId')
        .populate('hometeamId')
        .populate('pitchId')
        // .skip((request.pageIndex > 0) ? (request.pageIndex - 1) * request.pageSize : 0)
        // .limit(request.pageSize)
        .exec().then(function (listTeam) {
            for (let value of listTeam) {
                var element = {
                    matchId: value._id,
                    homeTeam: value.hometeamId.name,
                    homeLogo: value.hometeamId.logo,
                    awayLogo: value.awayteamId.logo,
                    awayTeam: value.awayteamId.name,
                    time: value.datetime,
                    type: value.type,
                    pitch: value.pitchId ? value.pitchId.name : '',
                    status: value.status
                };
                var checkUserTeam = false;
                for (let valueUser of value.awayteamId.members) {
                    if (valueUser.userId.equals(request.userId)) {
                        checkUserTeam = true;
                        break;
                    }
                }
                for (let valueUser of value.hometeamId.members) {
                    if (valueUser.userId.equals(request.userId)) {
                        checkUserTeam = true;
                        break;
                    }
                }
                if (checkUserTeam == true) {
                    list.push(element);
                }
                // if(list.length == request.pageSize) break; 
            }
            console.log(list);

            list.sort(function (a, b) {
                return (a.time - b.time);
            });
            return Promise.resolve(list);
        }).catch(function (err) {
            return Promise.reject({
                statusCode: 500,
                message: failMessage.match.create.systemErr
            });
        });

}



