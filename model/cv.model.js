var mongoose = require('mongoose');
var Schema = mongoose.Schema;

const cvSchema = new Schema({
    certificate:[{
        university: String,
        major: String,
        certificate: String,
        from: Date,
        to: Date
    }],
    experience: [{
        company: String,
        position: String,
        from: Date,
        to: Date
    }],
    language: [{
        language: String,
        typeL: String
    }],
    user:{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'user'
    }
});

var cv = mongoose.model('cv', cvSchema);
module.exports = cv;
