var mongoose = require('mongoose');
var deepPopulate = require('mongoose-deep-populate')(mongoose);
mongoose.plugin(deepPopulate);
var Schema = mongoose.Schema;

var matchSchema = new Schema({
    hometeamId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'team'
    },
    awayteamId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'team'
    },
    pitchId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'pitch'
    },
    datetime: {
        type: Date
    },
    type: {
        type: String
    },
    status: {
        type: String
    },
    result: {
        homteamResult: {
            type: Number
        },
        awayteamResult: {
            type: Number
        }
    }
});
var match = mongoose.model('match', matchSchema);

module.exports = match;
