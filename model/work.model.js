var mongoose = require('mongoose');
var Schema = mongoose.Schema;

const workSchema = new Schema({
    name: String,
    dateTime: Date,
    position: String,
    salary: {
        salaryFrom: {
            type: Number,
            default: 0
        },
        salaryTo: {
            type: Number,
            default: 0
        },
        type: {
            type: String,
            default: 'VND'
        },
        averageSalary:{
            type: Number,
            default:0
        }
    },
    requirement: String,
    benifit: String,
    major: String,
    company: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'company',
    },
    description: String,
    cvSent : [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'cv'
    }]
});
workSchema.index({ name: 'text', requirement: 'text', company: 'text' },{"weights": {name: 3,requirement:2,company:1}});

var work = mongoose.model('work', workSchema);
module.exports = work;



