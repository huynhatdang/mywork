var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var pitchSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    image: {
        type: String
    },
    address: {
        type: String,
        required: true
    },
    location: {
        latitude: {
            type: Number
        },
        longitude: {
            type: Number
        }
    },
    hotline: {
        type: String,
        required: true
    },
    quanlity: {
        type: Number,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    priceList: [{
        time: {
            type: String,
            required: true
        },
        price: {
            type: Number,
            required: true
        },
        promotion: {
            type: String,
            required: true
        },
        status: {
            type: String,
            required: true
        }
    }],
    feedback: [{
        userId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'user'
        },
        vote: {
            type: Number,
            required: true
        },
        comment: String
    }]
});

var pitch = mongoose.model('pitch', pitchSchema);
module.exports = pitch;
