var mongoose = require('mongoose');
var Schema = mongoose.Schema;

const companySchema = new Schema({
    name:{
        type: String,
        required: true
    },
    major:{
        type: String,
        required: true
    },
    image: {
        type: String,
        required: true
    },
    detailName:{
        type: String,
        required: true
    },
    contact: {
        type: Number,
        required: true
    },
    address: {
        address: {
            type: String
        },
        location: {
            latitude: Number,
            longitude: Number
        }
    },
    dateTime: Date,
    human: {
        type: String,
        required: true
    },
    scale: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    website: {
        type: String,
    },
    work: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'work'
    }]
});


var company = mongoose.model('company', companySchema);
module.exports = company;


