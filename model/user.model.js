var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var userSchema = new Schema({
    email: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    company: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'company'
    },
    address: {
        address: {
            type: String
        },
        location: {
            latitude: Number,
            longitude: Number
        }
    },
    cv:{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'cv'
    },
    salt: {
        type: String
    },
    username: {
        type: String,
        required: true,
        unique: true
    },
    roles: {
        type: String
    },
    name:{
        type: String,
        required: true
    },
    favorite:[{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'work'
    }]
});
var user = mongoose.model('user', userSchema);

module.exports = user;