var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var teamSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  date: {
    type: Date,
  },
  logo: {
    type: String
  },
  colours: {
    type: String
  },
  gallary: {
    type: String
  },
  score :{
    type: Number,
    default:1
  },
  requestJoinTeam: [{
    userId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'user'
    }
  }],
  message:[{
    message:String,
    createdTime: Date
  }],
  members: [{
    userId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'user'
    },
    role: {
      type: String
    },
    position: {
      type: String
    },
    phone: {
      type: String
    },
    email: {
      type: String
    },
    firstname: {
      type: String
    },
    lastname: {
      type: String
    }
  }],
  location: {
    latitude: {
      type: Number
    },
    longitude: {
      type: Number
    }
  }
});
var team = mongoose.model('team', teamSchema);
module.exports = team;
