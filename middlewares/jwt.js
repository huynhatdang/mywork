var jwt = require('jsonwebtoken');
var constant = require('./../services/constant');
module.exports = {
    authentication: authentication
};

function authentication(roleArray) {
    const role = Array.prototype.slice.call(roleArray);

    return (req, res, next) => {
        console.log(req.body);
        // console.log(req.body);
        // check header or url parameters or post parameters for token
        var token = req.body.token || req.query.token || req.headers['x-access-token'];
        // decode token
        if (token) {
            // verifies secret and checks exp
            jwt.verify(token, process.env.SECRET_KEY, function (err, decoded) {
                if (err) {
                    console.log('error');
                    return res.json({ success: false, message: 'Failed to authenticate token.' });
                } else {
                    // console.log(decoded.roles,constant.role.COMPANY,role);
                    if (decoded.roles === constant.role.USER && role.indexOf(constant.role.USER) > -1) {
                        req.user = decoded;
                        next();
                    } else if (decoded.roles === constant.role.COMPANY && role.indexOf(constant.role.COMPANY) > -1) {
                        req.user = decoded;
                        next();
                    }
                    else {
                        res.status(405);
                        res.json({
                            message: 'Permission'
                        });
                    }
                }
            });
        } else {
            // if there is no token
            // return an error
            return res.status(403).send({
                success: false,
                message: 'No token provided.'
            });
        }
    }
}